const path = require('path');

module.exports = {
  'presets': [
    ['@babel/preset-env',
      {
        "modules": false,
        "useBuiltIns": "usage",
        "corejs": 3,
        "debug": false
      }
    ]
  ],
  'env': {
    'test': {
      'plugins': [
        ['@babel/plugin-transform-modules-commonjs'],
      ]
    }
  },
  'plugins': [
    ["@babel/plugin-syntax-dynamic-import"],
    ["@babel/plugin-transform-async-to-generator"],
    ["@babel/plugin-proposal-decorators", { "legacy": true }],
    ["@babel/plugin-proposal-class-properties", { "loose": true }],
  ]
};
