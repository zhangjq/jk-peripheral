# equipme-plugins

## 介绍
健康管家插件

## 使用
1. 安装
```javascript
"dependencies": {
  //...
  "equipme-plugins": "git+ssh://git@gitlab.pajk-ent.com:research_iot/html-iot.git#6ab0f868"
}
```

2. h5再引入蓝牙模块
```javascript
  require('equipme-plugins/libs/ble_access');
```

3. webpack配置
```javascript
    {
      test: /\.(jsx?|tsx?)$/,
      loader: 'babel-loader',
      options: {
        configFile: resolve('babel.config.js')
      },
      include: [
        path.resolve(__dirname, 'src'),
        // 需要增加对equipme-plugins的编译
        path.resolve(__dirname, "node_modules/equipme-plugins")
      ]
  }
```

## version

#### 1.0.0
1. A 初始版本
2. A 体脂秤
