import FatScale, { MEASURE_STATUS } from "@equipme/category/FatScale";
import { getLogger } from "@equipme/libs/logger";

const { expect } = require('chai');

const logger = getLogger('scale.spec');

mocha.setup('bdd');


const familyId = "2238150939020708";

describe('体脂秤', async () => {
  let fatScale;

  before(function() {
    let height = 175,
      gender = 1,
      birthday = '1991-01-01',
      personId = familyId;
    fatScale = new FatScale();

    fatScale.setPerson({ height, gender, birthday, personId });
  });



  it('蓝牙状态', async () => {
    let res = await fatScale.getBluetoothAdapterState();
    const { available, discovering } = res;
    expect(available || !discovering).to.be.true;
  });


  it('启动插件打开蓝牙', async () => {
    let res = await fatScale.start();
    expect(res.errCode).to.equal(0);
  }).timeout(2000);


  it('扫描并连接设备', async () => {
    let res = await fatScale.discovery();
    expect(res.errCode).to.equal(0);
  }).timeout(4000);

  it('解码请求', async () => {
    const hexString = '6706A6D3DD3A34F2B0EE8698A69AAE243CCE8694';
    const info = fatScale.getPerson();
    const decRes = await fatScale.decodeReport(hexString, info, fatScale.getDeviceSku());
    logger.debug('[解码报告]', decRes);
  }).timeout(5000);


  it('开始测量', async () => {
    const reportReq = () => {
      return new Promise((resolve, reject) => {
        const offDataChange = fatScale.onDataChange(data => {
          let { code, weight } = data;
          switch (code) {
            case MEASURE_STATUS.MEASURING:
              logger.debug('正在测量', weight);
              break;
            case MEASURE_STATUS.PAUSE:
              logger.debug('空闲');
              break;
            case MEASURE_STATUS.REPORT:
              const reportData = data.reportData;
              logger.debug('测量完成', reportData, new Date());

              offDataChange();
              resolve(reportData);
              break;
            default:
              logger.debug(`###${code}###`, weight, reportData);
              break;
          }
        });
      });
    };

    const reportData = await reportReq();

    const { weight, measurement } = JSON.parse(reportData.reportDetail);
    const { bmi } = fatScale.achiveReport(measurement);

    expect(reportData.code).to.equal(0);

    logger.debug('测量完成bmi', bmi, weight);
    expect(bmi).to.be.above(0);
  }).timeout(50000);


});
