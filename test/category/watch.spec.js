
import BPWatch from "@equipme/category/BPWatch";

import { getLogger } from "@equipme/libs/logger";

const logger = getLogger('BPWatch', { actived: true });

mocha.setup('bdd');

describe('血压表', async () => {

  const bpMonitor = new BPWatch();

  it('蓝牙状态', async () => {
    let res = await bpMonitor.getBluetoothAdapterState();
    const { available, discovering } = res;
    expect(available || !discovering).to.be.true;
  });

  
  it('启动插件打开蓝牙', async () => {
    let res = await bpMonitor.start();
    expect(res.errCode).to.equal(0);
  }).timeout(5000);

  
  it('扫描并连接设备', async () => {
    let res = await bpMonitor.discovery();
    expect(res.errCode).to.equal(0);
  }).timeout(6000);

  
  it('开始测量', async () => {
    const reportReq = () => {
      return new Promise((resolve, reject) => {
        const offDataChange = fatScale.onDataChange(data => {
          logger.debug('-----onDataChange: ', data);
          // let { code, weight } = data;
          // switch (code) {
          //   case MEASURE_STATUS.MEASURING:
          //     logger.debug('正在测量', weight);
          //     break;
          //   case MEASURE_STATUS.PAUSE:
          //     logger.debug('空闲');
          //     break;
          //   case MEASURE_STATUS.REPORT:
          //     const reportData = data.reportData;
          //     logger.debug('测量完成', reportData, new Date());

          //     offDataChange();
          //     resolve(reportData);
          //     break;
          //   default:
          //     logger.debug(`###${code}###`, weight, reportData);
          //     break;
          // }
        });
      });
    };

    return;
    const reportData = await reportReq();

    const { weight, measurement } = JSON.parse(reportData.reportDetail);
    const { bmi } = fatScale.achiveReport(measurement);

    expect(reportData.code).to.equal(0);

    logger.debug('测量完成bmi', bmi, weight);
    expect(bmi).to.be.above(0);
  }).timeout(50000);



});