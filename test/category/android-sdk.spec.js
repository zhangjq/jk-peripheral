import { getLogger } from "@equipme/libs/logger";
// import { promisify } from "../utils/util";

const chai = require('chai');
const { expect } = chai;

const logger = getLogger('android-sdk.spec');

mocha.setup('bdd');

describe('Android sdk测试', async() => {
  var devicesArray;
  it('1.openBluetoothAdapter开启蓝牙', async()=> {
    logger.debug("debug");
    logger.log("log");
    var process = () => {
      new Promise((reslove, reject) => {
        var begin=new Date();
        PAG_NATIVE.openBluetoothAdapter({
          success: function(res){
            var end=new Date();
            var time=end-begin;
            logger.debug(" 开启蓝牙耗时："+time+"ms");
            logger.log("开启蓝牙:",res);
            expect(res.errCode).to.equal(0);
            reslove();
          },
          fail: function (res){
            logger.log(res);
            reject();
          },
          complete: function(res){
            logger.log("开启蓝牙complete");
          }
        });
      });
    };
    await process();
  });

  it('测试应该2000毫秒后结束', function(done) {
    var x = true;
    var f = function() {
      x = false;
      expect(x).to.be.not.ok;
      done(); // 通知Mocha测试结束
    };
    setTimeout(f, 2000);
  }).timeout(4000);

  // it("4.onBluetoothAdapterStateChange-监听蓝牙适配器状态变化事件", () => {
  //   var handle = function(res) {
  //     logger.log("监听蓝牙适配器状态变化事件",res);
  //     expect(res.available).to.be.a('boolean');
  //     expect(res.discovering).to.be.a('boolean');
  //   };

  //   const spy = chai.spy(handle);

  //   PAG_NATIVE.onBluetoothAdapterStateChange(spy);

  //   setTimeout(() => {
  //     expect(spy).to.have.been.called();
  //   }, 300);
  // });

  it('5.startBluetoothDevicesDiscovery扫描蓝牙设备', async()=>{
    logger.debug("5.startBluetoothDevicesDiscovery扫描蓝牙设备");
    var process = () => {
      new Promise((reslove, reject) => {
        var begin=new Date();
        PAG_NATIVE.startBluetoothDevicesDiscovery({
          allowDuplicatesKey:true,
          success(res){
            var end = new Date();
            var time = end - begin;
            logger.debug("开启扫描蓝牙耗时：" + time + "ms");
            logger.log("扫描蓝牙设备:",res);
            expect(res.errCode).to.equal(0);
            reslove();
          },
          fail(res){
            logger.error("开始扫描蓝牙设备失败！",res);
            expect(false).to.be.true;
            reject();
          },
          complete(res){
            logger.log("扫描蓝牙设备-complete");
          }
        });
      });
    };
    await process();
  });

  it('测试应该2000毫秒后结束', function(done) {
    var x = true;
    var f = function() {
      x = false;
      expect(x).to.be.not.ok;
      done(); // 通知Mocha测试结束
    };
    setTimeout(f, 2000);
  }).timeout(4000);

  it('8.onBluetoothDeviceFound监听寻找到新设备的事件',async()=>{
    logger.debug("8.onBluetoothDeviceFound监听寻找到新设备的事件");
    // PAG_NATIVE.onBluetoothDeviceFound(function(devices) {
    //   // logger.log(ab2hex(devices[0].advertisData))
    //   if (devices.length > 0){
    //     logger.log(devices[0].name);
    //   }
    //   // console.dir(devices)
    // });
    var handle = function(res) {
      logger.log('设备>>>>>>'+ res.devices.length,res);
      // logger.log(res.devices);
      // logger.debug("devicesId:"+res.devices[0].deviceId,"name:" + res.devices[0].name);
    };
    PAG_NATIVE.onBluetoothDeviceFound(handle);
  });

  it('3.getBluetoothAdapterState蓝牙状态-开始扫描', async()=>{
    var process = () => {
      new Promise((reslove, reject) => {
        var begin=new Date();
        PAG_NATIVE.getBluetoothAdapterState({
          success(res){
            var end = new Date();
            var time = end - begin;
            logger.debug("获取蓝牙状态耗时："+ time + "ms");
            logger.log("蓝牙状态-开始扫描:",res);
            if(res){
              expect(true).to.be.true;
            }else{
              expect(false).to.be.false;
            }
          },
          fail(res){
            logger.error("蓝牙状态-开始扫描-失败：",res);
            expect(false).to.be.true;
          },
          complete(res){
            logger.log("蓝牙状态:complete");
          }
        });
      });
    };
    await process();
  });

  it('测试应该4000毫秒后结束', function(done) {
    var x = true;
    var f = function() {
      x = false;
      expect(x).to.be.not.ok;
      done(); // 通知Mocha测试结束
    };
    setTimeout(f, 4000);
  }).timeout(6000);

  it('7.getBluetoothDevices获取在蓝牙模块生效期间所有已发现的蓝牙设备', async()=>{
    logger.debug("7.getBluetoothDevices获取在蓝牙模块生效期间所有已发现的蓝牙设备");
    var process = () => {
      new Promise((reslove, reject) => {
        var begin=new Date();
        PAG_NATIVE.getBluetoothDevices({
          success(res) {
              var end = new Date();
              var time = end - begin;
              logger.debug("获取设备列表耗时：" + time + "ms");
              logger.log("设备列表长度：",res.devices.length);
              devicesArray = res.devices;
              for (var i = 0; i < res.devices.length; i++){
                logger.log(res.devices[i].deviceId);
                logger.log(res.devices[i].name);
              }
              // if (res.devices[0]) {
              //   logger.log(ab2hex(res.devices[0].advertisData))
              //   // expect(res.errCode).to.equal(0);
              // }
              reslove();
            },
            fail(res){
              logger.error("获取列表失败:",res);
              expect(false).to.be.true;
              reject();
            },
            complete(res){
              logger.log("获取列表方法complete",res);
            }
        });
      });
    };
    await process();
  });

  it('6.stopBluetoothDevicesDiscovery-停止扫描', async()=>{
    var process = () => {
      new Promise((reslove, reject) => {
      var begin =  new Date();
      PAG_NATIVE.stopBluetoothDevicesDiscovery({
        success(res){
          var end = new Date();
          var time = end - begin;
          logger.debug("停止扫描耗时："+ time +"ms");
          logger.log("停止扫描成功:",res);
          expect(res.errCode).to.equal(0);
          reslove();
        },
        fail(res){
          logger.error("停止扫描失败");
          expect(false).to.be.true;
          reject();
        }
      });
      });
    };
    await process();
  });

  var deviceId;

  it("连接设备", async()=>{
    // if(!devicesArray){
    //   logger.error("devicesArray is null!");
    //   expect(false).to.be.true;
    //   return;
    // }
    for(var i = 0;i<devicesArray.length;i++){
      if(devicesArray[i].deviceId == "3C:A3:08:A8:9C:18"){
        logger.debug("device：：："+i,devicesArray[i].deviceId);
        deviceId = devicesArray[i].deviceId;
        break;
      }
    }
    if(!deviceId){
      logger.error("未找到");
      expect(false).to.be.true;
      return;
    }
    var process = () => {
      new Promise((reslove, reject) => {
        logger.debug("创建连接：deviceId:"+deviceId);
        PAG_NATIVE.createBLEConnection({
          deviceId,
          success (res) {
            logger.log("创建连接成功：",res);
            var deviceService;
            PAG_NATIVE.getBLEDeviceServices({
              // 这里的 deviceId 需要已经通过createBLEConnection 与对应设备建立链接
              deviceId,
              success (res) {
                logger.log('设备services:', res.services);
                if(res.services.length > 0){
                  deviceService = res.services[0];
                  var serviceId = deviceService.uuid;
                  PAG_NATIVE.getBLEDeviceCharacteristics({
                    // 这里的 deviceId 需要已经通过 createBLEConnection 与对应设备建立链接
                    deviceId,
                    // 这里的 serviceId 需要在 getBLEDeviceServices 接口中获取
                    serviceId,
                    success (res) {
                      logger.debug('characteristics>>>>>>>>>>>>:', res.characteristics);
                      for(var j = 0;j<res.characteristics.length;j++){
                        logger.debug("读取：",res.characteristics[j].properties.read);
                        logger.debug("写入：",res.characteristics[j].properties.write);
                        if(res.characteristics[j].properties.read && res.characteristics[j].properties.write){
                          logger.debug("可读可写的：",res.characteristics[j]);
                          var characteristicId = res.characteristics[j].uuid;
                          // break;
                          PAG_NATIVE.readBLECharacteristicValue({
                            deviceId,
                            serviceId,
                            characteristicId,
                            success(res){
                              console.log('readBLECharacteristicValue:', res.errCode)
                            }
                          });
                        }
                      }
                    },
                    fail(res){
                      logger.error("获取getBLEDeviceServices失败",res);
                    }
                  });
                }else{
                  logger.debug("res.deviceService is null");
                  return;
                }
              }
            });
          },
          fail (res){
            logger.log("创建连接失败：",res);
          }
        });
      });
    };
    await process();
  });
    
  it('测试应该3000毫秒后结束', function(done) {
    var x = true;
    var f = function() {
      x = false;
      expect(x).to.be.not.ok;
      done(); // 通知Mocha测试结束
    };
    setTimeout(f, 3000);
  }).timeout(5000);

  it("获取连接的设备", async()=>{
    var process = () => {
      new Promise((reslove, reject) => {
      var begin = new Date();
      PAG_NATIVE.getConnectedBluetoothDevices({
        success (res) {
          var end = new Date();
          var time = end - begin;
          logger.debug("获取连接的设备列表耗时："+ time + "ms");
          logger.log("获取处于已连接状态的设备:",res);
          var begin_stop = new Date();
          PAG_NATIVE.closeBLEConnection({
            deviceId,
            success (res) {
              var end_stop = new Date();
              var time_stop = begin_stop - end_stop;
              logger.debug("断开设备耗时："+ time + "ms");
              logger.log("断开设备连接",res);
              expect(res.errCode).to.be.equal(0);
              reslove();
            },
            fail (res){
              logger.debug("断开设备连接失败",res);
              reject();
            }
          });
        },
        fail (res){
          logger.error("获取处于已连接状态的设备-失败:",res);
          expect(res.errCode).to.be.equal(0);
        }
      });
    });
    };
    await process();
  });


  it('2.closeBluetoothAdapter关闭蓝牙', async()=>{
    var process = () => {
      new Promise((reslove, reject) => {
      var begin = new Date();
      PAG_NATIVE.closeBluetoothAdapter({
        success (res) {
          var end = new Date();
          var time = end - begin;
          logger.debug("关闭蓝牙耗时："+ time + "ms");
          logger.log('关闭蓝牙',res);
          expect(res.errCode).to.equal(0);
        },
        fail (res){
          logger.error("关闭蓝牙失败:",res);
          expect(false).to.be.true;
        }
      });
    });
  };
  await process();
  });

  it('getBluetoothAdapterState蓝牙状态-关闭', async()=>{
    var process = () => {
      new Promise((reslove, reject) => {
        var begin=new Date();
        PAG_NATIVE.getBluetoothAdapterState({
          success(res){
            var end = new Date();
            var time = end - begin;
            logger.debug("获取蓝牙状态耗时："+ time + "ms");
            logger.log("蓝牙状态-关闭:",res);
            if(res){
              expect(true).to.be.true;
            }else{
              expect(false).to.be.false;
            }
          },
          fail(res){
            logger.error("蓝牙状态-关闭-失败：",res);
            expect(false).to.be.true;
          },
          complete(res){
            logger.log("蓝牙状态-关闭:complete");
          }
        });
      });
    }
    await process();
  });

});