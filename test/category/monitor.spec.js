
import BPMonitor from "@equipme/category/BPMonitor";

const { expect } = require('chai');

mocha.setup('bdd');

const { expect } = require('chai');

describe('心电心率仪', async () => {

  const bpMonitor = new BPMonitor();

  it('蓝牙状态', async () => {
    let res = await bpMonitor.getBluetoothAdapterState();
    const { available, discovering } = res;
    expect(available || !discovering).to.be.true;
  });

  
  it('启动插件打开蓝牙', async () => {
    let res = await bpMonitor.start();
    expect(res.errCode).to.equal(0);
  }).timeout(5000);

  
  it('扫描并连接设备', async () => {
    let res = await bpMonitor.discovery();
    expect(res.errCode).to.equal(0);
  }).timeout(6000);

  
  it('开始测量', async () => {
    
  });




});