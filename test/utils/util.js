export function promisify(api) {
  return (options, ...params) => {
    return new Promise((resolve, reject) => {
      api(Object.assign({}, options, { success: resolve, fail: reject }), ...params);
    });
  };
}

export async function sleep(duration) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, duration);
  });
}


// 判断是不是 Android
export function isAndroid() {
  var isAndroid = /(android)/i.test(navigator.userAgent);
  return isAndroid;
}

// 判断是不是 IOS9
export function isIOS() {
  var isIOS = /(iphone|ipad|ipod)/i.test(navigator.userAgent);
  return isIOS;
}

export function isWxApp() {
  var isWxApp = typeof wx === "object" && typeof getAPP === "function";
  return isWxApp;
}

export function loadjs(script_filename) {
  var script = document.createElement('script');
  script.setAttribute('type', 'text/javascript');
  script.setAttribute('src', script_filename);
  script.setAttribute('id', 'coolshell_script_id');

  var script_id = document.getElementById('coolshell_script_id');
  if(script_id){
      document.getElementsByTagName('head')[0].removeChild(script_id);
  }
  document.getElementsByTagName('head')[0].appendChild(script);
}
