require("mocha/mocha.js");
require("mocha/mocha.css");
require("./report/style.css");

const { loadjs, isAndroid } = require("./utils/util");


// const VConsole = require('vconsole');

const chai = require('chai');
const spies = require('chai-spies');

// todo: 
require('@equipme/libs/ble_access');

// require('@equipme/libs/hybrid/pajk_android_hybrid');
require('@equipme/libs/hybrid/pajk_ios_hybrid');

// if (isAndroid()) {
//   loadjs('pajk_android_hybrid.js');
// } else {
//   loadjs('pajk_ios_hybrid.js');
// }


chai.use(spies);

require('./category/scale.spec');
// require('./category/monitor.spec');

// require('./category/sdk.spec');
// require('./category/android-sdk.spec');

(global || window).handleStart = () => {
  mocha.run();
};

// var vConsole = new VConsole();