const { resolve, join } = require("path");
const chalk = require("chalk");
const webpack = require("webpack");
const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const { progressPluginFun } = require('./tools');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpackConfig = require("./webpack.dev.config.js")();


const basedir = resolve(__dirname, '../test/');
const distdir = resolve(__dirname, '../dist/');

const compiler = webpack(Object.assign({}, webpackConfig, {
  entry: {
    "main-test": join(basedir, 'main-test.js')
  },
  plugins: [
    ...webpackConfig.plugins,
    new CleanWebpackPlugin(distdir, {
      root: resolve(__dirname, '../'),
      verbose: true,
      dry: false
    }),
    new HtmlWebpackPlugin({
      template: join(basedir, 'report/index.ejs'),
      filename: 'index.html',
      inject: true
    }),
    new HtmlWebpackInlineSourcePlugin()
  ]
}));

// compiler.apply(new ProgressPlugin(progressPluginFun));

compiler.run((err, stats) => {
  if (err) throw err;
  process.stdout.write(stats.toString({
    color: true,
    modules: false,
    children: false,
    chunks: true,
    chunkModules: false
  }) + '\n');
});
