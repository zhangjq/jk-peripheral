const { resolve, join } = require("path");
const chalk = require("chalk");
const webpack = require("webpack");
const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const { progressPluginFun } = require('./tools');

const express = require('express');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const httpProxyMiddleware = require("http-proxy-middleware");


const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const webpackConfig = require("./webpack.config.js")();

const basedir = resolve(__dirname, '../test/');
const distdir = resolve(__dirname, '../dist/');

/* webpack */
const compiler = webpack(Object.assign({}, webpackConfig, {
  entry: {
    "main-test": join(basedir, 'main-test.js')
  },
  plugins: [
    ...webpackConfig.plugins,
    new HtmlWebpackPlugin({
      template: join(basedir, 'report/index.ejs'),
      filename: 'index.html',
      inject: true
    }),
    new HtmlWebpackInlineSourcePlugin(),
    new webpack.HotModuleReplacementPlugin()
  ]
}));

compiler.apply(new ProgressPlugin(progressPluginFun));


/* dev Server */
const app = express();

// 用 webpack-dev-middleware 启动 webpack 编译
app.use(webpackDevMiddleware(compiler, {
  publicPath: webpackConfig.output.publicPath,
  overlay: true,
  hot: true
}));

// 使用 webpack-hot-middleware 支持热更新
app.use(webpackHotMiddleware(compiler, {
  publicPath: webpackConfig.output.publicPath,
  noInfo: true
}));

// 添加静态资源拦截转发
app.use(express.static(resolve(__dirname, '../dist')));

// 代理接口
const proxyOptions = {
  target: 'https://api.test.pajkdc.com/m.api',
  logLevel: 'info',
  changeOrigin: true,
  secure: false
};
app.use('/m.api', httpProxyMiddleware(proxyOptions));

app.listen(8080, function(err) {
  if (err) return console.log(err);
  console.log('listen at',chalk.bgGreen("http://localhost:8080"));
});
