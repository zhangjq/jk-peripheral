// const glob = require('glob');
const fs = require('fs');
const { relative, resolve, join } = require("path");
const chalk = require("chalk");
const webpack = require("webpack");
const ProgressPlugin = require('webpack/lib/ProgressPlugin');
const { progressPluginFun } = require('./tools');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpackConfig = require("./webpack.config.js")();
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');

const APP_VERSION = fs.readFileSync("./VERSION", "utf-8").replace(/\r|\n/ig, "");
const basedir = resolve(__dirname, '../test/');
const distdir = resolve(__dirname, '../dist/');


const args = process.argv.slice(2);
const watch = !!(args.find(v => v.indexOf('watch') > -1));
const haserver = !!(args.find(v => v.indexOf('server') > -1));


// function getEntry(rootSrc) {
//   var map = {};
//   console.log('-------', rootSrc);
//   glob.sync(rootSrc + '/category/**/index.js')
//     .forEach(file => {
//       var key = relative(rootSrc, file).replace('.js', '');
//       map[key] = file;
//     });
//   return map;
// }

const libsentry = {
  pajk_android_hybrid: 'libs/hybrid/pajk_android_hybrid.js',
  pajk_ios_hybrid: 'libs/hybrid/pajk_ios_hybrid.js',
  ble_access: 'libs/ble_access.js',
  fat_scale: 'category/FatScale/index.js'
};

const output = {
  pathinfo: false,
  path: distdir,
  library: '[name]',
  globalObject: 'this',
  // libraryExport: 'default',
  libraryTarget: 'umd',
  filename: `${APP_VERSION}/[name].js?`// env.esnext ? 'var' : 'umd',
};


const compiler = webpack(Object.assign({}, webpackConfig, {
  entry: libsentry,
  output,
  plugins: [
    ...webpackConfig.plugins,
    new CleanWebpackPlugin(`${distdir}/${APP_VERSION}/`, {
      root: resolve(__dirname, '../'),
      verbose: true,
      dry: false
    }),
    /* new HtmlWebpackPlugin({
      template: join(basedir, 'report/libs.ejs'),
      filename: 'libs.html',
      inject: true
    }),
    new HtmlWebpackInlineSourcePlugin(),
    new webpack.HotModuleReplacementPlugin() */
  ]
}));

compiler.apply(new ProgressPlugin(progressPluginFun));

const stdout = (err, stats) => {
  if (err) throw err;
  process.stdout.write(stats.toString({
    color: true,
    modules: false,
    children: false,
    chunks: true,
    chunkModules: false
  }) + '\n');
};

// server
if (haserver) {
  const express = require('express');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');

  /* dev Server */
  const app = express();
  // 用 webpack-dev-middleware 启动 webpack 编译
  app.use(webpackDevMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath,
    overlay: true,
    hot: true
  }));

  // 使用 webpack-hot-middleware 支持热更新
  app.use(webpackHotMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath,
    noInfo: true
  }));

  // 添加静态资源拦截转发
  app.use(express.static(resolve(__dirname, '../dist')));

  app.listen(8081, function(err) {
    if (err) return console.log(err);
    console.log('listen at',chalk.bgGreen("http://localhost:8080"));
  });

} else {
  compiler.run(stdout);
}

/* if (watch) {

  
} else {
 
} */
