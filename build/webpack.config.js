const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const { resolve } = path;

const distdir = path.join(__dirname, '../dist');

const autoconfig = require('../autoconfig.js');
const env = ((autoconfig.URL) && (!/\.(dev)\./.test(autoconfig.URL)) ? "production" : "development");

// const APP_VERSION = fs.readFileSync("./VERSION", "utf-8").replace(/\r|\n/ig, "");


module.exports = () => {

  const isProd = true;// env !== "development";

  const definePlugin = new webpack.DefinePlugin({
    __DEV__: !isProd
  });

  const webpackConf = {
    mode: !isProd ? 'development' : 'production',
    /* entry: {
        app: path.resolve(__dirname, 'src/main.js')
    }, */
    devtool: !isProd ? 'source-map' : false,
    output: {
      pathinfo: false,
      path: distdir,
      filename: !isProd ? '[name].js?' : '[name].[chunkhash:6].js'
    },
    watch: !isProd,
    plugins: [
      definePlugin,
    ],
    optimization: { }, 
    module: {
      rules: [{
          test: /\.(js|ts)$/,
          loader: 'babel-loader',
          options: {
            configFile: resolve('babel.config.js')
          },
          exclude: /node_modules/,
          include: [
            resolve(__dirname, '../src'),
            resolve(__dirname, '../test'),
          ]
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        }, {
          test: /\.(png|jpg|jpeg|gif)$/,
          exclude: /node_modules/,
          use: [{
            loader: 'url-loader'
          }]
        }
      ]
    },
    resolve: {
      extensions: ['.js', '.jsx', '.ts', '.tsx'],
      modules: [
        resolve(__dirname, '../src'),
        resolve(__dirname, '../node_modules')
      ],
      alias: {
        "@equipme": resolve(__dirname, '../src'),
      }
    }
  };

  // if (isProd) {
    Object.assign(webpackConf.optimization, {
      minimizer: [
        new UglifyJsPlugin({
          sourceMap: false,
          uglifyOptions: {
            compress: {
              unused: true,
              dead_code: true, // big one--strip code that will never execute
              // warnings: false, // good for prod apps so users can't peek behind curtain
              drop_debugger: true,
              drop_console: true,
            },
          },
        }),
      ],
    });
  // }

  return webpackConf;
};
