import { install as installShim } from "../libs/shim";
// import PAG_NATIVE from "../libs/hybrid/ble_access_dev";

const phera = (() => {
  let api;
  if (typeof wx === "object" && typeof getAPP === "function") {
    api = wx;
  } else if (typeof PAG_NATIVE === "object") {
    api = PAG_NATIVE;
  } else {
    throw new Error('缺少环境变量');
  }
  installShim(api);

  return api;
})();


export default phera;
