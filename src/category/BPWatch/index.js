import { command, serviceId, notifyCharacteristic, writeCharacteristic } from "./const";
import { promisify } from "../../libs/util";
import Peripheral from "../Peripheral";
import { ab2hex, str2ab } from "../../libs/util";
import phera from "../../const/phera";
import { okmsg, failmsg } from "../../const/msg";
import { getLogger } from "../../libs/logger";

const logger = getLogger('BPWatch', { actived: true });


/**
 * 血压手表
 */
class BPWatch extends Peripheral {

  constructor() {
    super();

    this.peripherals = {};
    this.connectedDeviceId = null;
    this.comState = "pause";

    this._allCValues = {};
    this.count = 0;
  }

  /**
   * 启动插件
   * @public
   * @returns {Promise<*>}
   */
  async start() {
    this.notifyBluetoothAdapterStateChange();
    this.notifyConnectionStateChange();

    let res = await this.getBluetoothAdapterState();
    console.log('[start]', res);
    let { discovering, available } = res;
    if (discovering === false) {
      let res = await (promisify(phera.openBluetoothAdapter))();
      console.log('[start#res]', res);
      return Promise.resolve(res.res || res);
    } else if (available) {
      return Promise.resolve(okmsg);
    } else {
      return Promise.reject(failmsg);
    }
  }

  /**
   * 开始扫描设备列表
   * @param {Function} cb 回调函数，参数未设备列表
   */
  async scanStart(cb) {
    let { discovering, available } = await this.getBluetoothAdapterState();
    if (!discovering) {
      let res = await (promisify(phera.startBluetoothDevicesDiscovery))();
      console.log(`[startScan]`, res);
    }
    let res = await (promisify(phera.getBluetoothDevices))();
    let { devices } = res;
    this.handleGetDevices(devices);

    this.onBluetoothDeviceFound();
  }

  /**
   * 开始扫描设备列表
   * @private
   */
  async discovery() {
    this.notifyDeviceFound();

    let { discovering } = await this.getBluetoothAdapterState();
    if (discovering === true) {
      return Promise.resolve(okmsg);
    } else {
      return promisify(phera.startBluetoothDevicesDiscovery)();
    }
  }

  /**
   * 发现设备
   * @private
   */
  notifyDeviceFound() {
    if (this._initedDeviceFound) {
      return;
    }
    this._initedDeviceFound = true;
    phera.onBluetoothDeviceFound(async (res) => {
      const devices = res.devices;
      if (!Array.isArray(devices)) return;

      for (let deviceItem of devices) {
        const { RSSI, advertisData, deviceId } = deviceItem;
        const localName = deviceItem.localName || deviceItem.name;

        if (localName.indexOf('BPW1') > -1) {
          console.log('----onBluetoothDeviceFound: ', deviceItem, deviceId);
          await this.scanStop();
          this.connect(deviceId);
        }
      }

    });
  }

  /**
   * 发现设备
   * @private
   */
  onBluetoothDeviceFound() {
    phera.onBluetoothDeviceFound(res => {
      console.log('[onBluetoothDeviceFound]', res);
      const { devices } = res;
      this.handleGetDevices(devices);
    });
  }

  /**
   * 处理新增设备
   * @private
   * @param {Array} devices 
   */
  handleGetDevices(devices) {
    Array.isArray(devices) && devices.forEach(device => {
      this.getEvents("__handleGetDevices").forEach(cb => {
        if (!device.name && !device.localName) {
          return;
        }
        console.log("[GetDevice]", device);
        const { name, deviceId } = device;
        if (name.indexOf("BPW") == -1) {
          return;
        }
        const peripherals = this.peripherals;
        if (!peripherals[deviceId]) {
          peripherals[deviceId] = device;
          cb(peripherals);
        }
      });
    });
  }

  /**
   * 监听血压手表列表
   * @public
   * @param {Function} cb 监听回调函数
   */
  notifyDeviceFound_bak(cb) {
    return this.addEvent('__handleGetDevices', cb);
    phera.onBluetoothDeviceFound(async (res) => {
      console.log('[onBluetoothDeviceFound]', res);

      const devices = res.devices;
      if (!Array.isArray(devices)) return;

      for (let deviceItem of devices) {
        const { RSSI, advertisData, deviceId } = deviceItem;
        const localName = deviceItem.localName || deviceItem.name;

        if (localName.indexOf('BPW1') > -1) {
          console.log('----onBluetoothDeviceFound: ', deviceItem, deviceId);
          await this.scanStop();
          this.connect(deviceId);
        }
      }
    });
  }

  notifyBluetoothAdapterStateChange() {
    phera.onBluetoothAdapterStateChange(res => {
      logger.log('[notifyBluetoothAdapterStateChange]', res);
      let { available, discovering } = res;
      this.available = available;
      this.discovering = discovering;

      if (available) {
        // this.dispatchDeviceChage({ errCode: 0, errMsg: 'available' });
        if (discovering === false) {
          this.discovery();
        }
        logger.log('蓝牙已打开');
      } else {
        this.currentDevice = null;
        this.dispatchDeviceChage({ errCode: 10001, errMsg: 'not available' });
        logger.log('蓝牙已关闭');
      }
    });
  }

  notifyConnectionStateChange() {
    phera.onBLEConnectionStateChange(res => {
      logger.log('[onBLEConnectionStateChange]', res);
      const { deviceId, connected } = res;
      this.connected = connected;
      if (!connected) {
        this.dispatchDeviceChage({ errCode: 10006, errMsg: 'no connection' });
        logger.log('设备已断开');
      } else if (this.currentDevice && deviceId === this.currentDevice.deviceId) {
        this.dispatchDeviceChage({ errCode: 0, errMsg: 'connectioned' });
        logger.log('设备重连接');
      }
    });
  }

  dispatchDeviceChage(params) {
    const events = this.getEvents('__onDeviceChange');
    Array.isArray(events) && events.forEach(cb => cb(params));
  }

  /**
   * 停止扫描设备
   * @public
   * @returns {Promise<*>} 调用状态
   */
  scanStop() {
    return promisify(phera.stopBluetoothDevicesDiscovery)();
  }

  /**
   * 连接设备并获取特征值
   * @public
   * @param {String} deviceId 设备Id
   * @returns {Promise<*>}
   */
  async connect(deviceId) {
    if (!deviceId) {
      console.log('[connect#peripherals]', this.peripherals);
      const peripherals = this.peripherals;
      const deviceIdlist = Object.keys(peripherals);
      // TODO: 选中设备
      deviceId = deviceIdlist[0];
    }

    return promisify(phera.createBLEConnection)({ deviceId })
      .then(async (res) => {
        console.error('-----connect: ', res);
        this.connected = true;
        this.connectedDeviceId = deviceId;
        let data = await promisify(phera.getBLEDeviceServices)({ deviceId });
        data = data.res || data;

        console.error('[connect#getBLEDeviceServices]', data);

        const { services } = data;

        const service = services[2],
          serviceId = service.uuid;
        this.serviceId = serviceId;

        let charData = await promisify(phera.getBLEDeviceCharacteristics)({
          deviceId,
          serviceId,
        });
        console.error('[connect#charData]', charData);
        let characteristic = charData.characteristics[0];
        let notifyCharacteristic = characteristic.uuid;
        let writeCharacteristic = charData.characteristics[1].uuid; // 写入数据时使用的uuid？？
        this.writeCharacteristic = writeCharacteristic;

        this.notifyCharacteristicValueChange();

        promisify(phera.notifyBLECharacteristicValueChange)({
          state: true, // 启用 notify 功能
          deviceId,
          // 这里的 serviceId 需要在 getBLEDeviceServices 接口中获取
          serviceId,
          // 这里的 characteristicId 需要在 getBLEDeviceCharacteristics 接口中获取
          characteristicId: notifyCharacteristic,
        });

        console.log('connect#resolve', res);
        // DEBUG: 关闭蓝牙
        // phera.closeBLEConnection({});
        // console.log('[connect#关闭蓝牙]');
        return Promise.resolve(res.res || res);
      }).catch(err => {
        console.error('connect#reject', err);
        phera.stopBluetoothDevicesDiscovery({});
        return Promise.reject(err);
      });
  }

  /**
   * 获取设备特征值
   * @private
   */
  getBLEDeviceCharacteristics() {
    const { connectedDeviceId: deviceId, serviceId } = this.state;
    return promisify(phera.getBLEDeviceCharacteristics)({ deviceId, serviceId });
  }

  /**
   * 开始测量
   * @public
   */
  async startMeasure() {
    return this.sendCommand(command.startCommand);
  }

  /**
   * 停止测量
   * @public
   */
  async stopMeasure() {
    return this.sendCommand(command.stopCommand);
  }

  /**
   * @private
   * @param {Array} commandcode
   * @returns {Promise<*>} 
   */
  sendCommand(commandcode) {
    const params = {
      deviceId: this.connectedDeviceId,
      serviceId: this.serviceId,
      characteristicId: this.writeCharacteristic,
      value: str2ab(commandcode)
    };
    console.error(`[sendCommand]`, params);
    return promisify(phera.writeBLECharacteristicValue)(params);
  }

  /**
   * 监听设备数据
   * @public
   * @param {Function} cb 监听回调函数
   * @returns {Function} 移除监听的函数
   */
  onDataChange(cb) {
    return this.addEvent('__onDataChange', cb);
  }

  /**
   * 检测特征值变化
   * @private
   */
  notifyCharacteristicValueChange() {
    phera.onBLECharacteristicValueChange(onNotityChangeRes => {
      // console.error('----onBLECharacteristicValueChange:', onNotityChangeRes);
      let characteristicValue = ab2hex(onNotityChangeRes.value);
      console.log('监听到特征值更新', characteristicValue, characteristicValue.length);
      // this._allCValues[characteristicValue] = {
      //   len: characteristicValue.length,
      //   count: this.count++,
      // }
      let res = this.parseValueChange(characteristicValue);
      this.getEvents('__onDataChange').forEach(cb => cb(res));
    });
  }

  /**
   * 结束插件
   * @public
   */
  async stop() {
    let deviceId = this.connectedDeviceId;
    phera.closeBLEConnection({ deviceId });
  }

  parseValueChange(characteristicValue) {
    if (characteristicValue.length == 20) {
      this.blood = characteristicValue;
    }
    if (characteristicValue.length == 8) {
      if (!this.blood) {
        console.error('none blood info.');
        return;
      }

      var year = "20" + parseInt(this.blood[6] + this.blood[7], 16);
      var month = parseInt(this.blood[8] + this.blood[9], 16);
      var day = parseInt(this.blood[10] + this.blood[11], 16);
      var hour = parseInt(this.blood[12] + this.blood[13], 16);
      var minute = parseInt(this.blood[14] + this.blood[15], 16);
      var time = year + "-" + (month < 10 ? '0' + month : month) + "-" + (day < 10 ? '0' + day : day) + " " + (hour < 10 ? '0' + hour : hour) + ":" + (minute < 10 ? '0' + minute : minute) + ":00";
      var systolic = parseInt(this.blood[18] + this.blood[19], 16);
      var diastolic = parseInt(characteristicValue[0] + characteristicValue[1], 16);
      var heartRate = parseInt(characteristicValue[2] + characteristicValue[3], 16);
      // 测量结果
      // wx.showModal({
      //   title: `您的测量结果`,
      //   content: `收缩压:${systolic}；舒张压：${diastolic};心率：${heartRate};测量时间：${time}`,
      //   showCancel: false
      // })
      // 
      console.log("收缩压:" + systolic + "； 舒张压：" + diastolic + "; 心率：" + heartRate + "; 测量时间：" + time);
      // var risk = util.getBloodPressEvaluation(systolic, diastolic);

      // var url = "/pages/index/blood?s=" + systolic + "&h=" + heartRate + "&d=" + diastolic + "&r=" + risk[0] + "&t=" + new Date().getTime();
      // that.uploadBlood(systolic, diastolic, heartRate, time);
    } else if (characteristicValue.length == 12) {
      try {
        // 测量血压
        var temp = parseInt(characteristicValue[8] + characteristicValue[9], 16);
        console.log('---实时数据： ', 0.005 * temp, temp + " mmHg");
      } catch (error) {
        console.error('测量时error: ', error);
      }
    }
  }
}

export default BPWatch;
