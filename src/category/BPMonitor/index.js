import { promisify } from "../../libs/util";
import Peripheral from "../Peripheral";
import { ab2hex, str2ab } from "../../libs/util";
import phera from "../../const/phera";
import { okmsg, failmsg } from "../../const/msg";
import { create } from "domain";
import { readcmd, channle } from "./const";

/**
 * 心电血压仪
 */
class BPMonitor extends Peripheral {

  constructor() {
    super();
    this.measureHeader = false;
    this.bNewMeasure = true;
  }

  async start() {
    return promisify(phera.openBluetoothAdapter)();
  }

  notifyDeviceFound() {
    phera.onBluetoothDeviceFound(async (res) => {
      const devices = res.devices;
      if (!Array.isArray(devices)) return;

      for (let deviceItem of devices) {
        const { RSSI, advertisData, deviceId } = deviceItem;
        const localName = deviceItem.localName || deviceItem.name;

        if (localName.indexOf('700X-000') > -1) {
          console.log('[onBluetoothDeviceFound]', res);
          await this.stopDiscovery();
          await this.scanStop();
          this.createConnect(deviceId);
        }
      }
    });
  }

  /**
   * 停止扫描设备
   * @public
   * @returns {Promise<*>} 调用状态
   */
  scanStop() {
    return promisify(phera.stopBluetoothDevicesDiscovery)();
  }

  async createConnect(deviceId) {
    const connectRes = await this.connect(deviceId);
    console.log('[createConnect#createBLEConnection]', connectRes);

    const servicesRes = await promisify(phera.getBLEDeviceServices)({ deviceId });
    console.log('[createConnect#getBLEDeviceServices]\n', deviceId, servicesRes);
    const serviceId = servicesRes.services.find(v => v.isPrimary).uuid;
    this.serviceId = serviceId;

    const characterRes = await promisify(phera.getBLEDeviceCharacteristics)({ deviceId, serviceId });
    console.log('[createConnect#getBLEDeviceCharacteristics]', characterRes);

    for (let item of characterRes.characteristics) {
      let { read, write, notify, indicate } = item.properties;
      if (read) {//该特征值是否支持 read 操作
        // console.log('该特征值是否支持 read 操作:', item.uuid);
        // this.readCharacteristicId = item.uuid;
      }
      if (write) {
        console.log('该特征值是否支持 write 操作:', item.uuid);
        this.writeCharacteristicId = item.uuid;
      }
      if (!write && (notify)) {//该特征值是否支持 notify或indicate 操作
        console.log('该特征值是否支持 notify或indicate 操作:', item.uuid);
        this.notifyCharacteristicId = item.uuid;
      }
    }

    const { readCharacteristicId, writeCharacteristicId, notifyCharacteristicId } = this.getCurrentServices();

    this.notifyCharacteristicValueChange();
    console.error('----', { readCharacteristicId, writeCharacteristicId, notifyCharacteristicId, deviceId, serviceId });

    return promisify(phera.notifyBLECharacteristicValueChange)({
      state: true, // 启用 notify 功能
      deviceId,
      serviceId,
      characteristicId: notifyCharacteristicId
    });
  }

  getCurrentServices() {
    const { serviceId, readCharacteristicId, writeCharacteristicId, notifyCharacteristicId } = this;
    return { serviceId, readCharacteristicId, writeCharacteristicId, notifyCharacteristicId };
  }

  notifyCharacteristicValueChange() {

    if (this._initedNotifyCharacterist) {
      return;
    }
    this._initedNotifyCharacterist = true;

    // setTimeout(()=>{
    //   console.error('关闭蓝牙连接');
    //   phera.closeBLEConnection({});
    // }, 5000);

    let seq = 0;
    phera.onBLECharacteristicValueChange(async (res) => {
      // console.error('-----', res);
      // return;
      let { deviceId, value } = res;

      const arr = new Uint8Array(value); //Uint8Array
      // console.log(`[notifyCharacteristicValueChange#Change]`, deviceId, arr);

      const arrI = new Int8Array(arr);
      const format = arr[1];

      if (arr[0] - seq != 1 && arr[0] - seq != -255) {
        // console.log('**************************');
      }
      seq = arr[0];

      switch (format) {
        case readcmd.BT_MEASURE:
          this.didUpdateMeasure(arr, arrI);
          break;
        case readcmd.BT_HEADER:
          this.didUpdateHeader(arr);
          break;
        default:
          break;
      }
    });
  }


  didUpdateMeasure(arr, arrI) {
    // console.log('[didUpdateMeasure]', arr);
    if (this.bNewMeasure) {
      this.bNewMeasure = false;
      this.measureHeader = true;
    }

    var samplingrate = 64;
    var bpPulseRate;
    var ecgCount, ecgSize;
    var ChannelData, bpDiastolic;

    for (var i = 4; i < 20;) {
      var ChannelNo = arr[i++] & 0xff;
      // var ChannelMSB = arr[i++];
      var ChannelMSB = arrI[i++];

      ChannelData = (arr[i++]) * 256;
      ChannelData = ChannelData + (arr[i++]);
      i += 4;
      if (ChannelNo == channle.CH_HR) {
        bpPulseRate = ChannelMSB * 256 * 256 + ChannelData;
        //hr = bpPulseRate;
        // that.setData({
        //   textLog: "HR=" + bpPulseRate,
        // });

      }
      if (ChannelNo == channle.CH_ECG) {
        //var iData;
        var MDRawData = ChannelMSB * 256 * 256 + ChannelData;

        if (ecgCount < samplingrate * 34) {
          rawData[ecgCount++] = (MDRawData);
          if (ecgCount > samplingrate * 3) {
            displayData[displayCount] = rawData[ecgCount - 1];
            displayCount++;

          }
          // that.setData({
          //   textLog: arr1,
          // });
        }

      }
      if (ChannelNo == channle.CH_MMHG) {
        bpDiastolic = ChannelMSB * 256 * 256 + ChannelData;

        // that.setData({
        //   textLog: "mmHg " + bpDiastolic,//count//displayData,
        // });

      }
      if (ChannelNo == channle.CH_AUTOSCALE) {
        ecgSize = ChannelMSB * 256 * 256 + ChannelData;
        // that.setData({
        //   ecgSize: ecgSize,
        // });

      }
    }//for
    console.error('[didUpdateMeasure.end-]', { bpDiastolic, ecgSize, ecgCount, bpPulseRate });

  }

  didUpdateHeader(arr) {
    var bpCmd;
    if (arr.length > 2) {
      bpCmd = arr[2];
    }
    console.log('[didUpdateHeader]', bpCmd, arr);
    if (bpCmd < 5 && this.measureHeader) {
      this.bNewMeasure = true;
      this.measureHeader = false;
      var header = new Int8Array(16);
      //var h;
      for (var i = 0; i < 16; i++) {
        header[i] = arr[i + 4];
        //h = arr[i + 4]+",";
      }
      var datetime = "20" + header[1] + "/" + header[2] + "/" + header[3] + "  " + header[4] + ":" + header[5] + ":" + header[6];
      var userMode = header[7];
      var analysisType = 0;
      if (header[10] != 0 && header[12] != 0)
        analysisType = 1;//BP
      else
        analysisType = 2;//ECG

      var noise = 0;
      if ((header[15] & 0x02) == 0x02)
        noise = 1;
      var isAF = 0;
      if ((header[15] & 0x80) == 0x80)//AF
        isAF = 1;
      else
        isAF = 0;
      var HeartRate, HighBloodPressure, LowBloodPressure, WHOIndicate, Pause, Rhythm, Fast, Slow;
      if (analysisType == 1) {
        HeartRate = (header[9]);
        HighBloodPressure = header[10] + header[11] * 256;
        LowBloodPressure = header[12] + header[13] * 256;
        WHOIndicate = header[14];
      }
      else if (analysisType == 2) {
        HighBloodPressure = '--';
        LowBloodPressure = '--';
        if (noise == 0) {
          HeartRate = (header[9]);
          if ((header[15] & 0x04) == 0x04)
            Rhythm = 1;
          else
            Rhythm = 0;
          if ((header[15] & 0x10) == 0x10)
            Pause = 1;
          else
            Pause = 0;
          if ((header[15] & 0x20) == 0x20)
            Fast = 1;
          else
            Fast = 0;
          if ((header[15] & 0x40) == 0x40)
            Slow = 1;
          else
            Slow = 0;

        }
        else
          HeartRate = "EE";

      }

      let textLog = datetime + "\n HeartRate=" + HeartRate + "\n SYS=" + HighBloodPressure + "\n DIA=" + LowBloodPressure;
      console.error(textLog);
    }

  }


}

export default BPMonitor;
