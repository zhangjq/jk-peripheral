/**
 * @typedef MeasureResult
 * @property {Number} code 业务码
 * @property {Object} measure 结果map
 * @property {Array} reportItemList 项目列表
 */

export const deviceSku = 'qingniu-bfs';

export const MEASURE_STATUS = {
  /**
   * 蓝牙未打开
   */
  NO_CONNECTION: 10006,
  /**
   * 发现设备
   */
  DEVICE_FOUND: 2008,
  /**
   * 正在连接
   */
  CONNECTING: 2009,
  /**
   * 已连接
   */
  CONNECTED: 2010,
  /**
   * 空闲
   */
  PAUSE: 2010,
  /**
   * 测量中
   */
  MEASURING: 3001,
  /**
   * 心率秤首次返回
   */
  SCALE_READ_MID: 3002,
  /**
   * 计算体质数据
   */
  CALC_PHYSICAL: 3004,
  /**
   * 心率秤第一次测量报告
   */
  REPORT_MID: 3005,
  /**
   * 秤测量返回
   */
  SCALE_READ: 3003,
  /**
   * 计算心率数据
   */
  CALC_HEART: 3006,
  /**
   * 获得测量报告
   */
  REPORT: 3010
};

