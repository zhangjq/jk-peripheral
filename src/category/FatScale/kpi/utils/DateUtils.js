module.exports = {
    getAge: (birthday)=>{
        // let birthday = '1987-01-12';
        birthday = birthday.split('-');
        let yearB = birthday[0];
        yearB = Number(yearB);
        let monthB = birthday[1];
        monthB = Number(monthB);
        let dateB = birthday[2];
        dateB = Number(dateB);

        let d = new Date();
        let year = d.getFullYear();
        let month = d.getMonth() + 1;
        let date = d.getDate();

        let age = year - yearB;
        let difMonth = month - monthB;
        let difDate = date - dateB;
        let valid = difMonth > 0 || ( difMonth == 0 && difDate >= 0 );
        if( !valid ){
            age--;
        }
        if(age < 0){
            age = 0;
        }
        return age;
    }
}