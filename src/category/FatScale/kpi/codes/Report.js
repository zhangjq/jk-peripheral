/**
 * Created by hdr on 17/8/17.
 */


const WeightReport = require('./Weight')
const BMIReport = require('./BMI')
const BodyfatReport = require('./Bodyfat')
const WaterReport = require('./Water')
const MuscleReport = require('./Muscle')
const MuscleMassReport = require('./MuscleMass')
const BoneReport = require('./Bone')
const LbmReport = require('./Lbm')
const ProteinReport = require('./Protein')
const SubfatReport = require('./Subfat')
const VisfatReport = require('./Visfat')
const BodyAgeReport = require('./BodyAge')
const BodyShapeReport = require('./BodyShape')
const BmrReport = require('./Bmr')
const HeartRateReport = require('./HeartRate')
const HeartIndexReport = require('./HeartIndex')
const theme = require("../theme.config.js")

const { dataValid} = require('../utils/util.js')

class Report {

  reportItemList = []

  constructor(measure) {
    this.measure = measure
    this._initReportItem()
  }

  get score() {
    return this.measure.score || 0
  }

  get measureTime() {
    return this.measure.time
  }

  isValid() {
    return dataValid(this.measure);
  }

  _initReportItem() {
    const arr = [];
    let reports = {
      WeightReport,
      HeartRateReport,
      HeartIndexReport,
      BMIReport,
      BodyfatReport,
      WaterReport,
      MuscleReport,
      MuscleMassReport,
      BoneReport,
      LbmReport,
      ProteinReport,
      SubfatReport,
      VisfatReport,
      BodyAgeReport,
      BodyShapeReport,
      BmrReport,
    }
    /* const reports = [
      WeightReport,
      HeartRateReport,
      HeartIndexReport,
      BMIReport,
      BodyfatReport,
      WaterReport,
      MuscleReport,
      MuscleMassReport,
      BoneReport,
      LbmReport,
      ProteinReport,
      SubfatReport,
      VisfatReport,
      BodyAgeReport,
      BodyShapeReport,
      BmrReport,
    ] */
    if (theme.reportSortList) {
      let obj = {}
      for(let key in theme.reportSortList) {
        obj[key] = reports[key]
      }
      reports = obj
    }
    //reports.forEach(report => {
    for (let key in reports) {
      let report = reports[key]
      if (report.isEnable(this.measure)) {
        const item = report.build(this.measure)
        // console.log("数据：", item)
        if (item) {
          if (item.fixLevel > 0 && item.value.toFixed) {
            item.value = parseFloat(item.value.toFixed(item.fixLevel))
            if (item.offset != 0) {
              item.offset = parseFloat(item.offset.toFixed(item.fixLevel))
            }
            if (item.boundaries && item.boundaries.length > 0)
              item.boundaries = item.boundaries.map(value => parseFloat(value.toFixed(item.fixLevel)))
          }
          if (item.showBar) {
            if (item.min > item.value) {
              item.min = item.value
            }
            if (item.max < item.value) {
              item.max = item.value
            }
          }
          arr.push(item)
        }
      }
    }
    //})
    this.reportItemList = arr
  }




}

module.exports = Report