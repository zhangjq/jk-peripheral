/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require( './MeasureItem')
const Calc = require( './Calc')
const UI = require( '../style/UI')

const STAND_LEVEL = 2

const BAR = [UI.color.report_highest, UI.color.report_higher, UI.color.report_stand, UI.color.report_higher]


module.exports = class ScoreReport{

  static build(measure) {
    const item = new MeasureItem()
    const value = measure.score
    const boundaries = [60, 80, 90]
    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)
    const isStand = level >= STAND_LEVEL

    item.name = "健康评分"
    item.boundaries = boundaries
    item.value = value
    item.levelNames = ["糟糕", "一般", "良好", "优秀"]
    item.standLevel = STAND_LEVEL;
    item.isStand = isStand
    item.level = level
    item.bar = BAR
    item.icon = 'report_score.png'
    item.fixLevel = 1
    item.typeValue = 15
    return item
  }


}