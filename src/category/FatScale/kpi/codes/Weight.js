/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require( './MeasureItem')
const Calc = require( './Calc')
const UI = require( '../style/UI')

const STAND_LEVEL = 2
const CONTENT_INFO = [
  "长期体重过轻会导致一系统列问题，如脱发、厌食症等，身体机能会下降，需要加强营养，多吃高蛋白食物，摄入更多的热量以增加体重。",
  "体重偏低，身体消瘦，建议加强营养，平衡饮食，多吃高蛋白食物，摄入更多的热量以增加体重。",
  "恭喜你拥有理想的体重，保持合理健康的生活方式，适量参加运动，你就可以维持标准体重了。",
  "体重偏重，略显肥胖，建议一周进行３-５次有氧运动，减少主食（米饭面食等）的摄入，增加高纤维粗粮比例。",
  "体重严重超标，建议低脂、低胆固醇、高纤维膳食，补充多种维生素，增加运动量进行体重控制。"]

const BAR = [UI.color.report_lowest, UI.color.report_lower, UI.color.report_stand, UI.color.report_higher, UI.color.report_highest]


module.exports = class WeightReport {

  static getStandWeight(gender, height) {
    if (gender == 0) {
      return ((height * 1.37) - 110) * 0.45;
    } else {
      return (height - 80) * 0.7;
    }
  }

  static calcScore(weight, gender, height) {
    let score = 0;
    const standWeight = WeightReport.getStandWeight(gender, height);
    const min = standWeight * 0.7;
    const max = standWeight * 1.3;

    if (weight == standWeight) { // 等于标准体重
      score = 100;
    } else if (weight > standWeight) { // 大于标准体重
      // 分两种情况 ：1. 实际体重大于最高的合格体重 , 2. 实际体重小于最高的合格体重
      if (weight > max)
        score = 50;
      else
        score = Calc.calcItemScore(standWeight, weight, max);
    } else if (weight < standWeight) { // 小于标准体重
      // 划分两种 ： 1. >0.7这个标准线上的体重 , 2. <= 0.7这个标准线上的体重
      if (weight > min)
        score = Calc.calcItemScore(standWeight, weight, min);
      else {
        // 分为六种情况：同获取男的体体重分数一样
        if (weight >= standWeight * 0.6)
          score = 50;
        else if (weight >= standWeight * 0.5)
          score = 40;
        else if (weight >= standWeight * 0.4)
          score = 30;
        else if (weight >= standWeight * 0.3)
          score = 20;
        else if (weight > 0)
          score = 10;
        else
          score = 0;
      }
    }
    return score;
  }

  static isEnable(measure){
    return true
  }

  static build(measure) {
    const item = new MeasureItem()
    const value = measure.weight
    const standWeight = WeightReport.getStandWeight(measure.gender, measure.height)
    const boundaries = [0.8, 0.9, 1.1, 1.2]
      .map(v => Number((standWeight * v).toFixed(2)));
    // [(standWeight * 0.8).toFixed(2), (standWeight * 0.9).toFixed(2), (standWeight * 1.1).toFixed(2), (standWeight * 1.2).toFixed(2)]
    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)
    const isStand = level == STAND_LEVEL

    item.name = "体重"
    item.boundaries = boundaries
    item.value = value
    item.levelNames = ["严重偏低", "偏低", "标准", "偏高", "严重偏高"]
    item.standLevel = STAND_LEVEL;
    item.isStand = isStand
    item.unit = "kg"
    item.desc = CONTENT_INFO[level]
    item.level = level
    item.bar = BAR
    item.fixLevel = 2
    item.typeValue = 1
    item.icon = 'report_weight.png'

    item.min = parseFloat((standWeight * 0.5).toFixed(2))
    item.max = parseFloat((standWeight * 1.5).toFixed(2))

    if (!isStand) {
      let offset = 0
      if (level < STAND_LEVEL) {
        offset = value - boundaries[STAND_LEVEL - 1]
      } else {
        offset = value - boundaries[STAND_LEVEL]
      }
      item.offset = Number(offset.toFixed(2));
      
    }


    return item
  }


}