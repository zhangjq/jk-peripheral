/**
 * 单个指标的描述对象，用以分析报告中的数据
 * Created by hdr on 17/8/11.
 */


module.exports = class MeasureItem {
  value
  name
  icon
  level = 0
  levelNames = []
  isStand = true
  unit = ""
  desc
  bar = []
  boundaries = []
  min = 0
  max

  offset = 0

  showBar = true

  fixLevel = 1;

  isBodyShape = false

  isExtend = false

  toggleExtend() {
    this.isExtend = !this.isExtend
  }

  get levelName() {
    if (this.levelNames.length > 0) {
      return this.levelNames[this.level]
    }
    return null
  }

  get levelColor() {
    if (this.bar.length > 0) {
      return this.bar[this.level]
    }
    return 0
  }
}