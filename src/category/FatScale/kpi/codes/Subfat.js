/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require( './MeasureItem')
const Calc = require( './Calc')
const UI = require( '../style/UI')

const STAND_LEVEL = 1


const CONTENT_INFO = [
  "适量的皮下脂肪能够保护内脏和抵御寒冷，适量增加高蛋白、高热量食物可以增加脂肪。",
  "你的皮下脂肪率处于标准范围。适当的运动量和合理的饮食就能保持适量的皮下脂肪。",
  "皮下脂肪过多是外表肥胖的表现主要原因，除了有氧减脂以外，多进行增肌训练，肌肉的增加可以让你拥有更完美的体形。"
]

const BAR = [UI.color.report_lower, UI.color.report_stand, UI.color.report_higher]

module.exports = class SubfatReport {

  static isEnable(measure) {
    return measure.origin.subfat_flag && measure.origin.subfat_flag === 1
  }

  static build(measure) {
    const value = measure.subfat
    if (!value || value == 0) {
      return null;
    }

    const item = new MeasureItem()
    const boundaries = measure.gender == 0 ? [18.5, 26.7] : [8.6, 16.7]

    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

    const isStand = level == STAND_LEVEL

    item.name = "皮下脂肪"
    item.boundaries = boundaries
    item.levelNames = ["偏低", "标准", "偏高"]
    item.standLevel = STAND_LEVEL;
    item.value = value
    item.isStand = isStand
    item.unit = "%"
    item.desc = CONTENT_INFO[level]
    item.level = level
    item.bar = BAR
    item.icon = 'report_subfat.png'
    item.typeValue = 4
    item.min = boundaries[0] - 7
    item.max = boundaries[1] + 7

    if (!isStand) {
      let offset = 0
      if (level < STAND_LEVEL) {
        offset = (value - boundaries[STAND_LEVEL - 1]).toFixed(2);
      } else {
        offset = (value - boundaries[STAND_LEVEL]).toFixed(2);
      }
      item.offset = Number(offset);
    }
    return item
  }

}