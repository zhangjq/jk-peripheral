/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require( './MeasureItem')
const Calc = require( './Calc')
const UI = require( '../style/UI')

const STAND_LEVEL = 1


const CONTENT_INFO = [
  "你的骨胳肌比率低于理想范围，跟多静态活动、不运动有关，会导致基础代谢率降低，腰酸背痛，力量下降，外在表现是发胖，也容易诱发心血管疾病。",
  "你的骨胳肌比率处于标准范围。运动量过少或者节食会导致肌肉流失，请保持适当的运动量和合理的饮食。",
  "如果脂肪比例正常，你是一个比较喜欢运动的人，适当的骨胳肌比率能够显示你健壮的体形，但过高的骨胳肌比率可能会影响你的灵活性。如果脂肪比例偏低，你的身材可能偏瘦，平衡身体各项参数，你就能拥有健康标准的身材。"
]
const DESC = "\n骨骼肌率：人体有多个肌肉组成，其中骨胳肌是可以通过锻炼增加的肌肉。";

const BAR = [UI.color.report_lower, UI.color.report_stand, UI.color.report_higher]

module.exports = class MuscleReport {

  static isEnable(measure) {
    return measure.origin.muscle_flag && measure.origin.muscle_flag === 1
  }

  static build(measure) {
    const value = measure.muscle
    if (!value || value == 0) {
      return null;
    }
    const item = new MeasureItem()
    const boundaries = measure.gender == 0 ? [40, 50] : [49, 59]

    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

    const isStand = level == STAND_LEVEL

    item.name = "骨骼肌率"
    item.boundaries = boundaries
    item.levelNames = ["偏低", "标准", "偏高"]
    item.standLevel = STAND_LEVEL;
    item.value = value
    item.isStand = isStand
    item.unit = "%"
    item.desc = CONTENT_INFO[level] + DESC
    item.level = level
    item.bar = BAR
    item.icon = 'report_muscle.png'
    item.typeValue = 7
    item.min = boundaries[0] - 10
    item.max = boundaries[1] + 10

    if (!isStand) {
      let offset = 0
      if (level < STAND_LEVEL) {
        offset = (value - boundaries[STAND_LEVEL - 1]).toFixed(2);
      } else {
        offset = (value - boundaries[STAND_LEVEL]).toFixed(2);
      }
      item.offset = offset
    }
    return item
  }

}