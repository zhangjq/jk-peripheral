/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require( './MeasureItem')
const Calc = require( './Calc')
const UI = require( '../style/UI')

const STAND_LEVEL = 1


const CONTENT_INFO = [
  "你的骨量水平偏低。长期低钙饮食、缺乏运动、过度减肥等都可能引起骨量偏低，多吃含钙高的食物，多晒太阳，多运动及时补钙。",
  "你的骨量水平标准。骨量在短期内不会出现明显的变化，你只要保证健康的饮食和适当的锻炼，就可以维持稳定健康的骨量水平。",
  "你的骨量水平偏高。说明骨骼中包含的钙等无机盐的含量非常充分，但要注意防范肾结石、低血压的风险，尽量避免高钙摄入。"
]
const BAR = [UI.color.report_lower, UI.color.report_stand, UI.color.report_higher]

module.exports = class Bone {

  static isEnable(measure) {
    return measure.origin.bone_flag && measure.origin.bone_flag === 1
  }

  static build(measure) {
    const value = measure.bone
    if (!value || value == 0) {
      return null;
    }
    const item = new MeasureItem()
    const weight = measure.weight
    let boundaries

    if (measure.gender == 1) {
      if (weight <= 60) {
        boundaries = [2.3, 2.7]
      } else if (weight < 75) {
        boundaries = [2.7, 3.1]
      } else {
        boundaries = [3.0, 3.4]
      }
    } else {
      if (weight <= 45) {
        boundaries = [1.6, 2.0]
      } else if (weight < 60) {
        boundaries = [2.0, 2.4]
      } else {
        boundaries = [2.3, 2.7]
      }
    }

    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

    const isStand = level == STAND_LEVEL

    item.name = "骨量"
    item.unit = "kg"
    item.fixLevel = 2
    item.boundaries = boundaries
    item.levelNames = ["偏低", "标准", "偏高"]
    item.standLevel = STAND_LEVEL;
    item.value = value
    item.isStand = isStand
    item.desc = CONTENT_INFO[level]
    item.level = level
    item.bar = BAR
    item.icon = 'report_bone.png'
    item.typeValue = 8
    item.mini = 0.5
    item.max = 4

    if (!isStand) {
      let offset = 0
      if (level < STAND_LEVEL) {
        offset = value - boundaries[STAND_LEVEL - 1]
      } else {
        offset = value - boundaries[STAND_LEVEL]
      }
      item.offset = Number(offset.toFixed(2));
    }
    return item
  }

}