/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require( './MeasureItem')
const Calc = require( './Calc')
const UI = require( '../style/UI')

const STAND_LEVEL = 1


const CONTENT_INFO = [
  "体水分率偏低，规律的饮食习惯和每天喝足8杯水可以维持正常的体水分水平，充足的水分可以促进代谢，带走废物和身体毒素。",
  "身体水分率处于标准值，适量饮水，适当运动，均衡饮食，保持身体水分的平衡。",
  "身体水分含量高，细胞活性高。充足的水分能帮助你更好地消化食物和吸收养分，并促进代谢，带走废物和毒素。",
]
const BAR = [UI.color.report_lower, UI.color.report_stand, UI.color.report_higher]

module.exports = class WaterReport {

  static isEnable(measure) {
    return measure.origin.water_flag && measure.origin.water_flag === 1
  }

  static build(measure) {
    const value = measure.water
    if (!value || value == 0) {
      return null;
    }
    const item = new MeasureItem()

    const boundaries = measure.gender == 1 ? [55, 65] : [45, 60]

    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

    const isStand = level >= STAND_LEVEL

    item.name = "体水分"
    item.boundaries = boundaries
    item.levelNames = ["偏低", "标准", "充足"]
    item.standLevel = STAND_LEVEL;
    item.value = value
    item.isStand = isStand
    item.unit = "%"
    item.desc = CONTENT_INFO[level]
    item.level = level
    item.bar = BAR
    item.icon = 'report_water.png'
    item.typeValue = 6
    item.min = boundaries[0] - 10
    item.max = boundaries[1] + 10

    if (!isStand) {
      let offset = value - boundaries[0]
      item.offset = Number(offset.toFixed(1));
    }
    return item
  }

}