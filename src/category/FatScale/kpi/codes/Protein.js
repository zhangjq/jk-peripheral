/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require( './MeasureItem')
const Calc = require( './Calc')
const UI = require( '../style/UI')

const STAND_LEVEL = 1


const CONTENT_INFO = [
  "蛋白质不⾜会引起基础代谢减少，也会引起肌肉的数量减少。坚持长期运动，适当提高肌肉⽐例，辅助于蛋⽩质的补充，可以提升蛋⽩质比例。",
  "你的蛋白质处于标准水平。",
  "蛋白质⽐例充⾜",
]
const DESC = "\n蛋白质:生命的物质基础，是构成细胞的基本有机物。"

const BAR = [UI.color.report_lower, UI.color.report_stand, UI.color.report_higher]

module.exports = class ProteinReport {

  static isEnable(measure) {
    return measure.origin.protein_flag && measure.origin.protein_flag === 1
  }

  static build(measure) {
    const value = measure.protein
    if (!value || value == 0) {
      return null;
    }
    const item = new MeasureItem()

    const boundaries = measure.gender == 0 ? [14, 16] : [16, 18]

    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

    const isStand = level >= STAND_LEVEL

    item.name = "蛋白质"
    item.boundaries = boundaries
    item.levelNames = ["偏低", "标准", "充足"]
    item.standLevel = STAND_LEVEL;
    item.value = value
    item.isStand = isStand
    item.unit = "%"
    item.desc = CONTENT_INFO[level] + DESC
    item.level = level
    item.bar = BAR
    item.icon = 'report_protein.png'
    item.typeValue = 11
    item.min = boundaries[0] - 7
    item.max = boundaries[1] + 7

    if (!isStand) {
      let offset = value - boundaries[0]
      item.offset = Number(offset.toFixed(0));
    }

    return item
  }

}