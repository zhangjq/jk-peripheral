/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require('./MeasureItem')
const Calc = require('./Calc')
const UI = require('../style/UI')

const STAND_LEVEL = 1


const CONTENT_INFO = [
    "心率超过低于40次，大多见于心脏病病人，常有心悸、胸闷、心前区不适，应及早检查，对症治疗。也有例外：心率低于40次的有的是很健康的人，长期从事重体力劳动和进行激烈运动的人，他们的心脏得到了锻炼，心跳次数比常人要少得多。",
    "窦性心动过缓。可见于长期从事体力劳动和运动员；病理性的见于甲状腺低下、颅内压增高、阻塞性黄胆、以及洋地黄、奎尼丁、或心得安类药物过量或中毒",
    "健康成人的心率为60～100次/分，大多数为60～80次/分，女性稍快",
    "窦性心动过速。常见于正常人运动、兴奋、激动、吸烟、饮酒和喝浓茶后；也可见于发热、休克、贫血、甲亢、心力衰竭及应用阿托品、肾上腺素、麻黄素等。",
    "心率超过160次，大多见于心脏病病人，常有心悸、胸闷、心前区不适，应及早检查，对症治疗。",
]
const DESC = "\n心率是指正常人安静状态下每分钟心跳的次数，也叫安静心率"

const BAR = [UI.color.report_lowest, UI.color.report_lower, UI.color.report_stand, UI.color.report_higher, UI.color.report_highest]

module.exports = class ProteinReport {

    static isEnable(measure) {
        return measure.heartRate && measure.heartRate > 0
    }

    static build(measure) {
        const value = measure.heartRate
        if (!value || value == 0) {
            return null;
        }
        const item = new MeasureItem()

        const boundaries = [40, 60, 100, 160]

        const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

        const isStand = level >= STAND_LEVEL

        item.name = "心率"
        item.boundaries = boundaries
        item.levelNames = ["过低", "偏低", "正常", '偏高', '过高']
        item.standLevel = STAND_LEVEL;
        item.value = value
        item.isStand = isStand
        item.unit = "bmp"
        item.desc = CONTENT_INFO[level] + DESC
        item.level = level
        item.bar = BAR
        item.icon = 'report_heart_rate.png'
        item.typeValue = 16
        item.min = 20
        item.max = 200

        if (!isStand) {
            let offset = 0
            if (level < STAND_LEVEL) {
                offset = value - boundaries[STAND_LEVEL - 1]
            } else {
                offset = value - boundaries[STAND_LEVEL]
            }
            item.offset = offset
        }

        return item
    }

}