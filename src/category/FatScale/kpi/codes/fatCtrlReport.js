const MeasureItem = require( './MeasureItem');
const Calc = require( './Calc');
const UI = require( '../style/UI');

const STAND_LEVEL = 1;

//脂肪控制
module.exports = class fatCtrlReport {
    static build(obj) {
        const item = new MeasureItem();
        let fatWeitght = obj.weight*obj.bodyfat/100;
        fatWeitght = Number(fatWeitght.toFixed(2));

        let boundaries = [];
        if (obj.gender == 1) {//男性
            boundaries = [11,21];
        } else {//女性
            boundaries = [21,31];
        }
        const level = Calc.calcLevel(fatWeitght, boundaries, STAND_LEVEL);
        
        item.name = "脂肪控制";
        item.unit = "kg";
        item.icon = 'report_fatCtrl.png';
        item.level = level;
        item.value = 0;//防止return，附一个值
        if (fatWeitght < boundaries[0]) {//脂肪偏少
            item.value = (boundaries[0] - fatWeitght).toFixed(2);
            item.desc = '需增加'+item.value+'kg';
        } else if(fatWeitght > boundaries[1]) {//脂肪偏高
            item.value = (fatWeitght - boundaries[1]).toFixed(2);
            item.desc = '需减少'+item.value+'kg';
        } else {
            item.desc = '维持当前即可';
        }
        return item;
    }
};
