/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require( './MeasureItem')
const Calc = require( './Calc')
const UI = require( '../style/UI')

const STAND_LEVEL = 0


const CONTENT_INFO = [
  "内脏脂肪指数标准，暂时没有太大风险。",
  "内脏脂肪指数偏高，持续保持均衡的饮食和适当的运动，以标准程度为目标，进行适当运动和限制卡路里。",
  "皮下脂肪过多是外表肥胖的表现主要原因，除了有氧减脂以外，多进行增肌训练，肌肉的增加可以让你拥有更完美的体形。"
]

const BAR = [UI.color.report_stand, UI.color.report_higher, UI.color.report_highest]

module.exports = class VisfatReport {

  static isEnable(measure) {
    return measure.origin.visfat_flag && measure.origin.visfat_flag === 1
  }

  static build(measure) {
    const value = measure.visfat

    if (!value || value == 0) {
      return null;
    }
    const item = new MeasureItem()
    const boundaries = [9, 14]

    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

    const isStand = level <= STAND_LEVEL

    item.name = "内脏脂肪等级"
    item.boundaries = boundaries
    item.levelNames = ["标准", "偏高", "严重偏高"]
    item.standLevel = STAND_LEVEL;
    item.value = value
    item.isStand = isStand
    item.unit = ""
    item.desc = CONTENT_INFO[level]
    item.level = level
    item.bar = BAR
    item.icon = 'report_visfat.png'
    item.typeValue = 5
    item.min = 0
    item.max = 20

    if (!isStand) {
      if (!isStand) {
        item.offset = value - boundaries[0]
      }
    }

    return item
  }

}