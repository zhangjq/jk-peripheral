/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require( './MeasureItem')
const Calc = require( './Calc')
const UI = require( '../style/UI')

const STAND_LEVEL = 1


const CONTENT_INFO = [
  "你缺少足够的肌肉，需要加强运动，增加肌肉。",
  "你的肌肉比较发达，继续保持。",
  "你的肌肉比较发达，继续保持。",
]
const DESC = "\n肌肉量：全身肌肉的重量，包括骨胳肌、心肌、平滑肌。";

const BAR = [UI.color.report_lower, UI.color.report_stand, UI.color.report_higher]

module.exports = class MuscleMassReport {


  static isEnable(measure) {
    return measure.origin.sinew_flag && measure.origin.sinew_flag === 1
  }

  static build(measure) {
    const value = measure.muscleMass
    if (!value || value == 0) {
      return null;
    }
    const item = new MeasureItem()
    const height = measure.height
    let boundaries

    if (measure.gender == 1) {
      if (height < 160) {
        boundaries = [38.5, 46.5]
      } else if (height <= 170) {
        boundaries = [44, 52.4]
      } else {
        boundaries = [49.4, 59.4]
      }
    } else {
      if (height < 150) {
        boundaries = [29.1, 34.7]

      } else if (height <= 160) {
        boundaries = [32.9, 37.5]
      } else {
        boundaries = [36.5, 42.5]
      }
    }
    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

    const isStand = level >= STAND_LEVEL

    item.name = "肌肉量"
    item.boundaries = boundaries
    item.levelNames = ["偏低", "标准", "充足"]
    item.value = value
    item.isStand = isStand
    item.unit = "kg"
    item.desc = CONTENT_INFO[level] + DESC
    item.level = level
    item.bar = BAR
    item.icon = 'report_muscle_mass.png'
    item.fixLevel = 2
    item.min = boundaries[0] - 5
    item.max = boundaries[1] + 5
    item.typeValue = 13
    if (!isStand) {
      let offset = value - boundaries[0]
      item.offset = Number(offset.toFixed(1));
    }

    return item
  }

}