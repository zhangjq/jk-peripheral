/**
 * Created by hdr on 17/8/17.
 */
const MeasureItem = require("./MeasureItem");
const Calc = require("./Calc");
const UI = require("../style/UI");
const DateUtils = require("../utils/DateUtils");

const STAND_LEVEL = 0

module.exports = class BodyAgeReport {

  static calcBodyAge(md) {
    const score = md.score;
    let curAge = DateUtils.getAge(md.birthday);
    let bodyAge = curAge;
    /**
     * score：分数 tempAge：体年龄 curAge:当前年龄
     */
    const dividers = [50, 60, 65, 67.5, 70, 72.5, 75, 80, 85, 87.5, 90, 92.5, 95, 96.5, 98, 99, 101]

    const lowest = 8;

    for (let i = 0; i < dividers.length; i++) {
      if (score < dividers[i]) {
        bodyAge = curAge + lowest - i;
        break;
      }
    }
    if (bodyAge <= 18) {
      bodyAge = 18;
    }
    return bodyAge;
  }


  static isEnable(measure) {
    return measure.origin.bodyage_flag && measure.origin.bodyage_flag === 1
  }

  static build(measure) {
    const value = measure.bodyAge
    if (!value || value == 0) {
      return null;
    }
    const item = new MeasureItem()
    const realAge = DateUtils.getAge(measure.birthday, measure.time)

    const boundaries = [realAge]

    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

    const isStand = value <= realAge

    item.name = "体年龄"
    item.value = value
    item.isStand = isStand
    item.unit = "岁"
    item.bar = [UI.color.report_stand, UI.color.report_higher]
    item.showBar = false
    item.levelNames = ["标准", "不标准"]
    item.standLevel = STAND_LEVEL;
    item.desc = "理想的体内年龄＝实际年龄　*　２ / ３，你还有年轻空间，加油！"
    item.level = level
    item.icon = 'report_bodyage.png'
    item.typeValue = 14
    if (!isStand) {
      // item.offset = value - realAge
      item.offset = Number((value - realAge).toFixed(1));
    }

    return item
  }

}