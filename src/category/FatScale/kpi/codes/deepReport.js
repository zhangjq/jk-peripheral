
import WeightReport from './Weight';
import BMIReport from './BMI';
import BodyfatReport from './Bodyfat';
import WaterReport from './Water';
import MuscleReport from './Muscle';
import MuscleMassReport from './MuscleMass';
import BoneReport from './Bone';
import LbmReport from './Lbm';
import ProteinReport from './Protein';
import SubfatReport from './Subfat';
import VisfatReport from './Visfat';
//import WaistHipReport from './WaistHip';
import BodyAgeReport from './BodyAge';
import BodyShapeReport from './BodyShape';
import BmrReport from './Bmr';
import HeartRate from './HeartRate';
import HeartIndex from './HeartIndex';

import ScoreReport from './Score';
import moment from '../libs/moment.js';
import UI from '../style/UI.js';
//import UnitUtils from '../utils/UnitUtils'

import fatCtrlReport from './fatCtrlReport'; // 脂肪控制
import fatWeightReport from './fatWeightReport'; // 脂肪重量
import muscleCtrlReport from './muscleCtrlReport'; // 肌肉控制
import inorganicSaltReport from './inorganicSaltReport'; // 无机盐控制

module.exports = class Report {

  _reportItemList = []

  static dataValid(measure) {
    return measure && measure.bodyfat > 5 && measure.bodyfat < 75;
  }
  

  constructor(measure) {
    this.measure = measure;
  }

  get score() {
    return this.measure.score || 0;
  }

  get measureTime() {
    return moment.unix(this.measure.time).format('YYYY-MM-DD HH:mm');
  }

  get reportItemList() {
    if (this._reportItemList.length === 0) {
      this._initReportItem();
    }

    return this._reportItemList;
  }

  get userId() {
    return this.measure.userId;
  }

  isValid() {
    return Report.dataValid(this.measure);
  }


  get scoreString() {
    const score = this.score;
    if (score === 0) {
      return {
        bigScore: "0",
        smallScore: '.0分'
      };
    }

    const f = (+score).toFixed(1) * 10;
    const scoreString = f + "分";
    const bigScore = scoreString.substr(0, scoreString.length - 2);
    const smallScore = "." + scoreString.substr(scoreString.length - 2, scoreString.length);
    return { bigScore, smallScore };
  }

  reloadItemList() {
    this._initReportItem();
  }

  _initReportItem() {
    const arr = [];
    const reports = [
      WeightReport,
      fatWeightReport,
      HeartRate,
      HeartIndex,
      BMIReport,
      BodyfatReport,
      WaterReport,
      MuscleReport,
      MuscleMassReport,
      BoneReport,
      fatCtrlReport,
      muscleCtrlReport,
      LbmReport,
      ProteinReport,
      inorganicSaltReport,
      SubfatReport,
      VisfatReport,
      BodyAgeReport,
      BodyShapeReport,
      BmrReport,
      ScoreReport
      //WaistHipReport,
    ];
    reports.forEach(report => {
      const item = report.build(this.measure);
      if (item) {
        if (item.unit === 'kg') {
          //重量单位
          //item.unit = UnitUtils.string
          //item.value = UnitUtils.convert(item.value)
          //item.offset = UnitUtils.convert(item.offset)
          //if (item.boundaries) {
          // console.log("正在转化", item.name, item.boundaries)
          //item.boundaries = item.boundaries.map(value => UnitUtils.convert(value))
          //}
        }

        if (item.value === undefined || item.value === null) {
          console.warn('不存在item value:', item);
          return;
        }

        if (item.fixLevel > 0 && item.value.toFixed) {
          item.value = parseFloat(item.value.toFixed(item.fixLevel));
        }
        if (item.showBar) {
          if (item.min > item.value) {
            item.min = item.value;
          }
          if (item.max < item.value) {
            item.max = item.value;
          }
          // console.log(`${item.name} 真实值${item.value} 最小值 ${item.min} 最大值 ${item.max}`)
        }


        arr.push(item);
      }
    });
    this._reportItemList = arr;
  }


  get bodyShapeIcon() {
    return BodyShapeReport.getIconWithShape(this.measure.bodyShape);
  }

  get bodyShapeItem() {
    return BodyShapeReport.build(this.measure);
  }

  get bodyfatMass() {
    const { weight, bodyfat } = this.measure;
    return (weight * bodyfat / 100).toFixed(2);
  }

  get weight() {
    return this.measure.weight || 0;
  }

  get bodyfat() {
    return this.measure.bodyfat || 0;
  }

  get muscle() {
    return this.measure.muscle || 0;
  }

  get muscleMass() {
    return this.measure.muscleMass || 0;
  }

  get controls() {
    const { gender, height, weight, bodyfat, } = this.measure;
    const standWeight = +WeightReport.getStandWeight(gender, height).toFixed(2);
    const standBodyfat = BodyfatReport.getStandBodyfat(gender);

    let weightControl, fatControl, muscleControl;

    if (weight > standWeight && bodyfat > (standBodyfat + 1)) {
      // 体重 > 标准体重； 体脂率 > 标准脂肪

      fatControl = parseFloat(((bodyfat - standBodyfat) * weight / 100).toFixed(2));

      muscleControl = parseFloat((fatControl / 3).toFixed(2));

      weightControl = parseFloat((fatControl - muscleControl).toFixed(2));

      fatControl = -fatControl;
      muscleControl = muscleControl;
      weightControl = -weightControl;

    } else if (weight < (standWeight - 1) && bodyfat < standBodyfat) {
      // 体重 < 标准体重； 体脂率 < 标准脂肪
      weightControl = parseFloat((standWeight - weight).toFixed(2));

      fatControl = parseFloat((weightControl / 3).toFixed(2));

      muscleControl = parseFloat((weightControl / 3 * 2).toFixed(2));

    } else {
      weightControl = 0;
      fatControl = 0;
      muscleControl = 0;
    }
    return {
      standWeight,
      weightControl,
      fatControl,
      muscleControl,
    };
  }

  get muscleLevel() {

    const muscleItem = MuscleReport.build(this.measure);
    return muscleItem.level;
  }

  get nutrition() {
    const proteinItem = ProteinReport.build(this.measure);
    const proteinLevel = proteinItem.isStand ? "正常" : "缺乏";
    const proteinColor = proteinItem.isStand ? UI.color.report_stand : UI.color.report_lower;

    const boneItem = BoneReport.build(this.measure);
    const boneLevel = boneItem.isStand ? "正常" : "缺乏";
    const boneColor = boneItem.levelColor;

    const bodyfatItem = BodyfatReport.build(this.measure);
    const levelStrings = ["缺乏", "正常", "过量", "过量"];
    const bodyfatLevel = levelStrings[bodyfatItem.level];
    const bodyfatColor = bodyfatItem.levelColor;

    return {
      proteinLevel,
      boneLevel,
      bodyfatLevel,
      proteinColor,
      boneColor,
      bodyfatColor,
    };
  }

  get waistHipLevel() {
    const { waist, hip, gender } = this.measure;
    if (waist == 0 || hip == 0) {
      return -1;
    }
    const whr = parseFloat((waist / hip).toFixed(1));
    if (gender == 0) {
      if (whr <= 0.6) {
        return 0;
      } else if (whr < 0.8) {
        return 1;
      } else {
        return 2;
      }
    } else {
      if (whr <= 0.7) {
        return 0;
      } else if (whr < 0.9) {
        return 1;
      } else {
        return 2;
      }
    }
  }

  get bodyfatItem() {
    return BodyfatReport.build(this.measure);
  }

  get bmiItem() {
    return BMIReport.build(this.measure);
  }

  get bmrLevel() {
    const bmrItem = BmrReport.build(this.measure);
    return bmrItem.level;
  }

  // get scoreLevel() {
  //   const scoreItem = Score.build(this.measure);
  //   return scoreItem.level
  // }

  get bodyAgeLevel() {
    const bodyAgeItem = BodyAgeReport.build(this.measure);
    return bodyAgeItem.level;
  }

};