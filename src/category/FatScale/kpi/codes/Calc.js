/**
 * Created by hdr on 17/8/16.
 */


class Calc {


  static calcItemScore(standValue, value, offsetValue) {
    const realityOffSet = Math.abs(standValue - value);
    const offSet = Math.abs(standValue - offsetValue);
    return 100 - (50 / offSet) * realityOffSet;
  }


  static calcLevel(value, boundaries, standLevel) {
    let level = 0;
    for (let i = 0; i < boundaries.length; i++) {
      if (value < boundaries[i] || (value == boundaries[i] && level == standLevel)) {
        break;
      }
      level++
    }
    return level
  }
}

module.exports = Calc