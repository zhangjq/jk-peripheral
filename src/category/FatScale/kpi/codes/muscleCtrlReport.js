const MeasureItem = require( './MeasureItem');

//肌肉控制
module.exports = class {
    static build(obj) {
        const item = new MeasureItem();

        let standard = [];
        if(obj.gender == 1){//男性
            if(obj.height < 160){
                standard = [38.5, 46.5];
            }else if(obj.height >= 160 && obj.height <= 170){
                standard = [44, 52.4];
            }else if(obj.height > 170){
                standard = [49.4, 59.4];
            }
        }else{//女性
            if(obj.height < 150){
                standard = [29.1, 34.7];
            }else if(obj.height >= 150 && obj.height <= 160){
                standard = [32.9, 37.5];
            }else if(obj.height > 160){
                standard = [36.5, 42.5];
            }
        }
        item.name = "肌肉控制";
        item.value = obj.muscleMass;
        item.unit = "kg";
        item.icon = 'report_muscleCtrl.png';
        if(obj.muscleMass < standard[0]){//肌肉偏少
            item.desc = '需增加'+(standard[0] - obj.muscleMass).toFixed(2)+'kg';
        }else{
            item.desc = '维持当前即可';
        }
        return item;
    }
}