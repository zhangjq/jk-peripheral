const WeightReport = require('./Weight')
const BMIReport = require('./BMI')
const BodyfatReport = require('./Bodyfat')
const WaterReport = require('./Water')
const MuscleReport = require('./Muscle')
const MuscleMassReport = require('./MuscleMass')
const BoneReport = require('./Bone')
const LbmReport = require('./Lbm')
const ProteinReport = require('./Protein')
const SubfatReport = require('./Subfat')
const VisfatReport = require('./Visfat')
const BodyAgeReport = require('./BodyAge')
const BodyShapeReport = require('./BodyShape')
const BmrReport = require('./Bmr')
const Report = require('./Report')
const Score = require('./Score')
const Calc = require('./Calc')
module.exports = {
  WeightReport,
  BMIReport,
  BodyfatReport,
  WaterReport,
  MuscleReport,
  MuscleMassReport,
  BoneReport,
  LbmReport,
  ProteinReport,
  SubfatReport,
  BodyAgeReport,
  BodyShapeReport,
  VisfatReport,
  BmrReport,
  Report,
  Score,
  Calc,
}