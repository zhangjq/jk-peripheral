/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require('./MeasureItem')
const Calc = require('./Calc')
const UI = require('../style/UI')

const STAND_LEVEL = 1


const CONTENT_INFO = [
  "低于正常范围很多会引起脑供血严重不足,记忆力减退等。具体的判断不能只依靠这一项的指标,需结合心电图及胸片的判断",
  "你的心脏指数处于标准水平。",
  "高于正常值说明泵血过强,容易引起心慌。具体的判断不能只依靠这一项的指标,需结合心电图及胸片的判断",
]
const DESC = "\n心脏泵血功能的评定,以对心脏功能状态进行评价。单位是：L/min/m^2（升/分钟/平方米）"

const BAR = [UI.color.report_lower, UI.color.report_stand, UI.color.report_higher]

module.exports = class ProteinReport {

  static isEnable(measure) {
    return measure.heartIndex && measure.heartIndex > 0
  }

  static build(measure) {
    const value = measure.heartIndex
    if (!value || value == 0) {
      return null;
    }
    const item = new MeasureItem()

    const boundaries = [2.5, 4.2]

    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

    const isStand = level >= STAND_LEVEL

    item.name = "心脏指数"
    item.boundaries = boundaries
    item.levelNames = ["偏低", "标准", "偏高"]
    item.standLevel = STAND_LEVEL;
    item.value = value
    item.isStand = isStand
    item.unit = "L/min/m^2"
    item.desc = CONTENT_INFO[level] + DESC
    item.level = level
    item.bar = BAR
    item.icon = 'report_heart_index.png'
    item.typeValue = 17
    item.min = 0
    item.max = 8

    if (!isStand) {
      let offset = 0
      if (level < STAND_LEVEL) {
        offset = value - boundaries[STAND_LEVEL - 1]
      } else {
        offset = value - boundaries[STAND_LEVEL]
      }
      item.offset = Number(offset.toFixed(1));
    }

    return item
  }

}