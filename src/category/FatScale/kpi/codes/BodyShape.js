/**
 * Created by hdr on 17/8/17.
 */
const MeasureItem = require("./MeasureItem")
const UI = require('../style/UI')

const BodyShape1 = 1 //隐形肥胖型
const BodyShape2 = 2 //运动不足型
const BodyShape3 = 3 //偏瘦型
const BodyShape4 = 4 //标准型
const BodyShape5 = 5 //偏瘦肌肉型
const BodyShape6 = 6 //肥胖型
const BodyShape7 = 7 //偏胖型
const BodyShape8 = 8 //标准肌肉型
const BodyShape9 = 9 //非常肌肉型

const STAND_LEVEL = 4;

const CONTENT_INFO = [
  "你的体型属于隐形肥胖型，得多进行有氧运动，否则很容易成为真胖子了。",
  "你的体型属于运动不足型，需要运动起来了。",
  "你的体型属于偏瘦型，需要加强营养了。",
  "你的体型属于标准型，继续保持。",
  "你的体型属于偏瘦肌肉型，继续保持。",
  "你的体型属于肥胖型，控制饮食和加强有氧运动能够助你降低脂肪。",
  "你的体型属于偏胖型，控制饮食和加强有氧运动能够助你降低脂肪。",
  "你的体型属于标准肌肉型，继续保持。",
  "你的体型属于非常肌肉型，继续保持。",
]
const BODYSHAPE_NAME_LIST = ["隐形肥胖型", "运动不足型", "偏瘦型", "标准型", "偏瘦肌肉型", "肥胖型", "偏胖型", "标准肌肉型", "非常肌肉型"]

const BAR = [UI.color.report_higher, UI.color.report_lower, UI.color.report_lower, UI.color.report_stand, UI.color.report_lower, UI.color.report_higher, UI.color.report_higher, UI.color.report_stand, UI.color.report_stand]

module.exports = class BodyShapeReport {

  static isEnable(measure) {
    return measure.origin.body_shape_flag && measure.origin.body_shape_flag === 1
  }

  //根据BMI、体脂率和性别获取体型
  static calcBodyShape(bodyfat, bmi, gender) {
    let bodyShape;
    if (gender == 1) {//男性
      if ((bmi < 18.5 && bodyfat > 26) ||
        (bmi >= 18.5 && bmi < 25.0 && bodyfat > 21.0)
      ) {
        bodyShape = BodyShape1;
      }
      else if (bmi < 18.5 && bodyfat >= 11.0 && bodyfat <= 26
      ) {
        bodyShape = BodyShape2;
      }
      else if (bmi < 18.5 && bodyfat < 11.0) {
        bodyShape = BodyShape3;
      }
      else if (bmi >= 18.5 && bmi <= 25.0 && bodyfat >= 11.0 && bodyfat <= 21.0) {
        bodyShape = BodyShape4;
      }
      else if (bmi >= 18.5 && bmi < 25.0 && bodyfat < 11.0) {
        bodyShape = BodyShape5;
      }
      else if (bmi >= 25.0 && bodyfat > 26
      ) {
        bodyShape = BodyShape6;
      }
      else if (bmi >= 25.0 && bodyfat > 21.0 && bodyfat <= 26
      ) {
        bodyShape = BodyShape7;
      }
      else if (bmi >= 25.0 && bmi < 30.0 && bodyfat <= 21.0) {
        bodyShape = BodyShape8;
      }
      else if (bmi >= 30.0 && bodyfat <= 21.0) {
        bodyShape = BodyShape9;
      }
    }
    else {//女性
      if ((bmi < 18.5 && bodyfat > 36.0) || (bmi >= 18.5 && bmi < 25.0 && bodyfat > 31.0)) {
        bodyShape = BodyShape1;
      }
      else if (bmi < 18.5 && bodyfat >= 21.0 && bodyfat <= 36.0) {
        bodyShape = BodyShape2;
      }
      else if (bmi < 18.5 && bodyfat < 21.0) {
        bodyShape = BodyShape3;
      }
      else if (bmi >= 18.5 && bmi <= 25.0 && bodyfat >= 21.0 && bodyfat <= 31.0) {
        bodyShape = BodyShape4;
      }
      else if (bmi >= 18.5 && bmi < 25.0 && bodyfat < 21.0) {
        bodyShape = BodyShape5;
      }
      else if (bmi >= 25.0 && bodyfat > 36.0) {
        bodyShape = BodyShape6;
      }
      else if (bmi >= 25.0 && bodyfat > 31.0 && bodyfat <= 36.0) {
        bodyShape = BodyShape7;
      }
      else if (bmi >= 25.0 && bmi < 30.0 && bodyfat <= 31.0) {
        bodyShape = BodyShape8;
      }
      else if (bmi >= 30.0 && bodyfat <= 31.0) {
        bodyShape = BodyShape9;
      }
    }
    return bodyShape;
  }

  static isEnable(measure) {
    return measure.origin.body_shape_flag && measure.origin.body_shape_flag === 1
  }

  static build(measure) {
    const value = measure.bodyShape
    if (!value || value == 0) {
      return null;
    }

    const item = new MeasureItem()

    const level = value - 1
    const isStand = level == 3 || level == 7 || level == 8

    item.name = "体型"
    item.isBodyShape = true
    item.levelNames = BODYSHAPE_NAME_LIST
    item.value = BODYSHAPE_NAME_LIST[level]
    item.bar = BAR
    item.isStand = isStand
    // item.standLevel = STAND_LEVEL;
    item.showBar = false
    item.desc = CONTENT_INFO[level]
    item.level = level
    item.icon = 'report_bodyshape.png'
    item.typeValue = 10
    return item
  }
  static getIconWithShape(bodyShape) {
    let bgImg, frImg;
    switch (bodyShape) {
      case BodyShape1: {
        bgImg = "invisible_fat_1.png"
        frImg = "invisible_fat_2.png"
        break;
      }
      case BodyShape2: {
        bgImg = "lack_sport_1.png"
        frImg = "lack_sport_2.png"
        break
      }
      case BodyShape3: {
        bgImg = "pianshou_1.png"
        frImg = "pianshou_2.png"
        break;
      }
      case BodyShape4: {
        bgImg = "standard_1.png"
        frImg = "standard_2.png"
        break;
      }
      case BodyShape5: {
        bgImg = "pianmuscle_1.png"
        frImg = "pianmuscle_2.png"
        break;
      }
      case BodyShape6: {
        bgImg = "fat_1.png"
        frImg = "fat_2.png"
        break;
      }
      case BodyShape7: {
        bgImg = "pianpang_1.png"
        frImg = "pianpang_2.png"
        break;
      }
      case BodyShape8: {
        bgImg = "standard_muscle_1.png"
        frImg = "standard_muscle_2.png"
        break;
      }
      case BodyShape9: {
        bgImg = "more_muscle_1.png"
        frImg = "more_muscle_2.png"
        break;
      }

    }
    return { bgImg, frImg }
  }
}
