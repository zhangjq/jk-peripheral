/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require( './MeasureItem')
const Calc = require( './Calc')
const UI = require( '../style/UI')

const STAND_LEVEL = 1


const CONTENT_INFO = [
  "当身体摄取到优质营养，并且令到小肠绒毛正常运作，就可以达到正常的脂肪比例。为了增重，食物最好以易消化、高蛋白、高热量为原则。",
  "目前你的体脂率处于标准范围，保持好的饮食方式和生活习惯是保持健康身材的最佳途径。",
  "要匀称不显胖，每日有氧运动要持续30分钟，体脂才会开始燃烧，快走、慢跑、游泳、爬楼梯、骑自行车都是很好的选择。",
  "你的体内囤积了太多脂肪，必须检测血压、血糖、肝功能等情况，是否潜藏危害。赶快开始你的减肥大战，坚持饮食控制、运动及改变生活方式。",
]
const BAR = [UI.color.report_lower, UI.color.report_stand, UI.color.report_higher, UI.color.report_highest]

const BODYFAT_MAX = 45;
const BODYFAT_MIN = 5;


module.exports = class BodyfatReport {

  static calcScore(bodyfat, gender) {
    let score = 0;
    const stand = gender == 1 ? 16 : 26;

    if (bodyfat == stand) {
      score = 100;
    } else if (bodyfat > stand) {
      if (bodyfat > BODYFAT_MAX)
        score = 50;
      else
        score = Calc.calcItemScore(stand, bodyfat, BODYFAT_MAX);
    } else {
      if (bodyfat <= BODYFAT_MIN) {
        if (0 < bodyfat && bodyfat <= 5)
          score = 10;
        else
          score = 0;
      } else {
        score = Calc.calcItemScore(stand, bodyfat, BODYFAT_MIN);
      }
    }
    return score;
  }

  static getStandBodyfat(gender) {
    return gender == 0 ? 22 : 15;
  }
  static isEnable(measure) {
    return true
  }


  static build(measure) {
    const value = measure.bodyfat
    if (!value || value == 0) {
      return null;
    }

    const item = new MeasureItem()

    const boundaries = measure.gender == 1 ? [11, 21, 26] : [21, 31, 36]

    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

    const isStand = level == STAND_LEVEL

    item.name = "体脂率"
    item.boundaries = boundaries
    item.levelNames = ["偏低", "标准", "偏高", "严重偏高"]
    item.standLevel = STAND_LEVEL;
    item.value = value
    item.isStand = isStand
    item.unit = "%"
    item.desc = CONTENT_INFO[level]
    item.level = level
    item.bar = BAR
    item.icon = 'report_bodyfat.png'
    item.typeValue = 3
    item.min = 5
    item.max = measure.gender == 1 ? 45 : 35

    if (!isStand) {
      let offset = 0
      if (level < STAND_LEVEL) {
        offset = (value - boundaries[STAND_LEVEL - 1]).toFixed(2);
      } else {
        offset = (value - boundaries[STAND_LEVEL]).toFixed(2);
      }
      item.offset = Number(offset);
    }

    return item
  }


}