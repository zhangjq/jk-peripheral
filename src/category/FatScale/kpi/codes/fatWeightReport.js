const MeasureItem = require( './MeasureItem');
const Calc = require('./Calc');
const UI = require('../style/UI');

const STAND_LEVEL = 1;

const BAR = [UI.color.report_lower, UI.color.report_stand, UI.color.report_higher, UI.color.report_highest];

// 脂肪重量
module.exports = class {
    static build(obj) {
        const item = new MeasureItem();

        let fatWeitght = obj.weight*obj.bodyfat/100;
        fatWeitght = fatWeitght.toFixed(2);
        if (isNaN(fatWeitght)){
          fatWeitght = 0;
        }

        let boundaries;
        if (obj.gender == 1) {
            boundaries = [11, 21, 26];
        } else {
            boundaries = [21, 31, 36];
        }

        item.levelNames = ["偏低", "标准", "偏高", "严重偏高"];

        const level = Calc.calcLevel(fatWeitght, boundaries, STAND_LEVEL);

        item.name = "脂肪重量";
        item.boundaries = boundaries;
        item.standLevel = STAND_LEVEL;
        item.value = fatWeitght;
        item.unit = "kg";
        item.bar = BAR;
        item.icon = 'report_fatWeitght.png';
        item.desc = '体内所含脂肪的重量';
        item.level = level;

        return item;
    }
};
