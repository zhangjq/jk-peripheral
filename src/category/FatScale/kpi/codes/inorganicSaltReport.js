import Bone from './Bone';
const MeasureItem = require( './MeasureItem');
const UI = require('../style/UI');


const BAR = [UI.color.report_lower,UI.color.report_stand , UI.color.report_higher];
// 无机盐控制
module.exports = class InorganicSaltReport {
  static build(obj) {
    let {weight, gender} = obj;
    const item = new MeasureItem();
    let boneData = Bone.build(obj);

    if (!boneData) {
      // console.warn('不存在测量项', obj, boneData);
      return null;
    }

    let value = boneData.value;
    let boundaries, desc;
    if(gender == 1){//男性
      if(weight <= 60){
        boundaries = [2.3, 2.7];
      }else if(weight >= 75){
        boundaries = [3, 3.4];
      }else{
        boundaries = [2.7, 3.1];
      }
    }else{ // 女性
      if(weight <= 45){
        boundaries = [1.6, 2];
      }else if(weight >= 60){
        boundaries = [2.3, 2.7];
      }else{
        boundaries = [2, 2.4];
      }
    }
    if(value < boundaries[0]){
      desc = '缺乏';
    }else if(value > boundaries[1]){
      desc = '充足';
    }else{
      desc = '正常';
    }
    
    item.bar = BAR;
    item.name = '无机盐';
    item.value = value;
    item.boundaries = boundaries;
    item.desc = desc;
    item.icon = 'report_salt.png'
    item.levelNames = ['缺乏','正常','充足'];
    item.standLevel = 0;
    return item;
  }
}