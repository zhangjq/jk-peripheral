/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require('./MeasureItem')
const UI = require('../style/UI')

const BAR = [UI.color.report_stand]

module.exports = class LbmReport {

  static isEnable(measure) {
    return measure.origin.fat_free_weight_flag && measure.origin.fat_free_weight_flag === 1
  }

  static build(measure) {
    const value = measure.lbm
    if (!value || value == 0) {
      return null;
    }
    const item = new MeasureItem()

    item.name = "去脂体重"
    item.levelNames = ["标准"]
    item.value = value
    item.isStand = true
    item.showBar = false
    item.unit = "kg"
    item.desc = '去脂体重是指除脂肪以外身体其他成分的重量，肌肉是其中的主要部分。通过该指标可以看出你锻炼的效果，也可以看出你减肥的潜力哦！'
    item.bar = BAR
    item.icon = 'report_lbm.png'
    item.fixLevel = 2
    item.typeValue = 12
    return item
  }

}