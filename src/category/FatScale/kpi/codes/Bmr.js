/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require('./MeasureItem')
const Calc = require('./Calc')
const UI = require('../style/UI')
const DateUtils = require('../utils/DateUtils')

const STAND_LEVEL = 1;

module.exports = class BmrReport {


  static getUnitBmr(gender, age) {
    let value;
    if (gender == 1) {
      if (age <= 15) {
        value = 46.7
      } else if (age <= 17) {
        value = 46.2
      } else if (age <= 19) {
        value = 39.7
      } else if (age <= 30) {
        value = 37.7
      } else if (age <= 40) {
        value = 37.9
      } else if (age <= 50) {
        value = 36.8
      } else {
        value = 35.6
      }
    } else {
      if (age <= 15) {
        value = 41.2
      } else if (age <= 17) {
        value = 43.4
      } else if (age <= 19) {
        value = 36.8
      } else if (age <= 30) {
        value = 35.0
      } else if (age <= 40) {
        value = 35.0
      } else if (age <= 50) {
        value = 34.0
      } else {
        value = 33.1
      }
    }
    return value;
  }

  static getStandBmr(height, gender, age, weight) {
    const unitBmr = BmrReport.getUnitBmr(gender, age)
    return ((0.0061 * height + 0.0128 * weight - 0.1529)
      * 24 * unitBmr - 80).toFixed(0);
  }

  static isEnable(measure) {
    return measure.origin.bmr_flag && measure.origin.bmr_flag === 1
  }

  static build(measure) {
    const value = measure.bmr
    if (!value || value == 0) {
      return null;
    }
    const item = new MeasureItem()
    const realAge = DateUtils.getAge(measure.birthday, measure.time)
    const standBmr = BmrReport.getStandBmr(measure.height, measure.gender, realAge, measure.weight)

    const boundaries = [standBmr]

    const level = Calc.calcLevel(value, boundaries, 1)

    const isStand = level == 1

    let desc
    if (isStand) {
      desc = "你的标准基础代谢率为" + standBmr
        + " kcal,处于达标状态。保持基础代谢率最有效的方式是每天都进行适量的运动。";
    } else {
      desc = "你的标准基础代谢率为" + standBmr
        + " kcal,目前处于未达标状态。持续轻量运动能够提高身体的基础代谢率，而节食基础代谢会大幅下降。";
    }

    item.name = "基础代谢"
    item.value = value
    item.isStand = isStand
    item.unit = "kcal"
    item.levelNames = ["不达标", "达标"]
    item.standLevel = STAND_LEVEL;
    item.bar = [UI.color.report_lower, UI.color.report_stand]
    item.showBar = false
    item.desc = desc
    item.level = level
    item.icon = 'report_bmr.png'
    item.typeValue = 9
    if (!isStand) {
      item.offset = value - standBmr
    }
    return item
  }

}