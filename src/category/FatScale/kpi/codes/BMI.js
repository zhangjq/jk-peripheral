/**
 * Created by hdr on 17/8/17.
 */

const MeasureItem = require('./MeasureItem')
const Calc = require('./Calc');

const UI = require('../style/UI')

const STAND_LEVEL = 1

const lowString = "需要提升体能健康增重，适当多吃高热量、高蛋白、高脂肪饮食，多做力量运动如举重、俯卧撑、仰卧起坐等。";
const middleString_male = "BMI达标，如果腰围也属于建议的尺寸男性（计算公式：身高(厘米) x 1/2-10=标准腰围(厘米）";
const middleString_female = "BMI达标，如果腰围也属于建议的尺寸女性（计算公式：身高(厘米) x 1/2 -13=标准腰围(厘米)）就更加理想了。";
const highString = "BMI超标，建议选择比较健康的方法减重，如控制饮食、改变不良生活习惯和参加跑步、跳绳、打篮球、踢足球等消耗体能的运动。";

const MALE_INFO = [lowString, middleString_male, highString]
const FEMALE_INFO = [lowString, middleString_female, highString]

const DESC = "BMI：是指身体质量指数，国际上常用的衡量人体胖瘦程度以及是否健康的一个标准。";

const BAR = [UI.color.report_lower, UI.color.report_stand, UI.color.report_higher]

const BMI_STAND = 22;
const BMI_MAX = 35;
const BMI_MIN = 9;

module.exports = class BMIReport {

  static calcBmi(height, weight) {
    height = height / 100;
    return (weight / (height * height)).toFixed(1);
  }

  static calcScore(bmi) {
    let score = 0;
    if (bmi == BMI_STAND) {
      score = 100.0;
    } else if (bmi > BMI_STAND) {
      if (bmi >= BMI_MAX)
        score = 50;
      else
        score = Calc.calcItemScore(BMI_STAND, bmi, BMI_MAX);

    } else {
      if (bmi > 15 && bmi < 22) {
        score = Calc.calcItemScore(BMI_STAND, bmi, BMI_MIN);
      } else {
        if (bmi >= 10)
          score = 40;
        else if (bmi >= 5)
          score = 30;
        else if (bmi >= 0)
          score = 20;
        else
          score = 0;
      }
    }
    return score
  }
  static isEnable(measure){
    return true
  }
  static build(measure) {
    const item = new MeasureItem()
    const value = measure.bmi

    const boundaries = [18.5, 25]
    const level = Calc.calcLevel(value, boundaries, STAND_LEVEL)

    const isStand = level == STAND_LEVEL

    item.name = "BMI"
    item.boundaries = boundaries
    item.value = value
    item.levelNames = ["偏低", "标准", "偏高"]
    item.standLevel = STAND_LEVEL;
    item.isStand = isStand
    item.unit = ""
    item.desc = (measure.gender == 0 ? FEMALE_INFO[level] : MALE_INFO[level]) + "\n\n" + DESC
    item.level = level
    item.bar = BAR
    item.icon = 'report_bmi.png'
    item.typeValue = 2
    item.min = 5
    item.max = 35

    if (!isStand) {
      let offset = 0
      if (level < STAND_LEVEL) {
        offset = value - boundaries[STAND_LEVEL - 1]
      } else {
        offset = value - boundaries[STAND_LEVEL]
      }
      item.offset = Number(offset.toFixed(1));
    }

    return item
  }


}