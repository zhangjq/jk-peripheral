import "../../libs/ble_access";
import "./scale/QN";
import { ab2hex, promisify, isIOS, isNumber, sleep } from "../../libs/util";
import Peripheral from "../Peripheral";
import phera from "../../const/phera";
import { request, checkError } from "../../libs/req";
import { MEASURE_STATUS, deviceSku } from "./const";
import { mapi } from "../../libs/api";
import { getLogger } from "../../libs/logger";
import { okmsg, oldmsg } from "../../const/msg";

export { MEASURE_STATUS };

const ST = MEASURE_STATUS;
const logger = getLogger('FatScale', { actived: true });

/**
 * 体脂秤
 * @extends {Peripheral}
 */
class FatScale extends Peripheral {

  /**
   * @typedef personInfo
   * @property {String} personId Id
   * @property {Number} height 身高
   * @property {Number} gender 1 男 0 女
   * @property {String} birthday 生日
   */


  /**
   * 启动插件
   * @returns {Promise<statusInfo>} 启动状态
   */
  async start() {
    logger.log('[start#0]');
    return new Promise((resolve, reject) => {
      if (typeof phera.setBluetoothIdentifer === "function") {
        phera.setBluetoothIdentifer(deviceSku);
      }
      if (typeof phera.checkVersion === "function") {
        if (!phera.checkVersion()) {
          logger.error('版本过低');
          return reject(oldmsg);
        }
      }
      promisify(phera.closeBluetoothAdapter)()
        .finally(err => {
          this._start()
            .then(res => resolve(res))
            .catch(err => reject(err));
        });
    });
  }

  _start() {
    return new Promise((resolve, reject) => {
      promisify(phera.openBluetoothAdapter)()
        .then(async (res) => {
          logger.log('[start#1]', res);
          await sleep(60);
          this.notifyBluetoothAdapterStateChange();
          await sleep(60);
          this.started = true;
          resolve(res);
        })
        .catch(err => {
          logger.error('[start#2]', err);
          reject(err);
        });
    });
  }

  /**
   * 开始扫描设备列表
   * @private
   */
  async discovery() {
    const connRes = await promisify(phera.getConnectedBluetoothDevices)();
    const { devices } = connRes;

    let device = this.findQNDevice(devices);
    if (device) {
      logger.log('已有连接设备');
      return;
    }

    logger.log('[discovery#0]');
    await sleep(100);
    this.notifyDeviceFound();
    // await sleep(100);
    // logger.log('[discovery#1]');
    // let { discovering } = await this.getBluetoothAdapterState();
    // logger.log('[discovery#3]', discovering);
    // if (discovering === true) {
    //   return Promise.resolve(okmsg);
    // } else {
    await sleep(100);
    return promisify(phera.startBluetoothDevicesDiscovery)();
    // }
  }


  /**
   * 监听蓝牙打开状态
   * @private
   */
  notifyBluetoothAdapterStateChange() {
    phera.onBluetoothAdapterStateChange(res => {
      logger.log('[notifyBluetoothAdapterStateChange]', res);
      let { available, discovering } = res;
      this.available = available;
      this.discovering = discovering;

      if (available) {
        // this.dispatchDeviceChage({ errCode: 0, errMsg: 'available' });
        if (discovering === false) {
          // this.discovery();
        }
        logger.log('蓝牙已打开');
      } else if (available === false) {
        this.currentDevice = null;
        this.dispatchDeviceChage({ code: 10001, msg: 'not available' });
        logger.log('蓝牙已关闭');
      }
    });
  }

  /**
   * 监听蓝牙连接
   * @private
   */
  notifyConnectionStateChange() {
    phera.onBLEConnectionStateChange(res => {
      logger.log('[onBLEConnectionStateChange]', res);
      const { deviceId, connected } = res;
      this.connected = connected;
      if (!connected) {
        this.dispatchDeviceChage({ code: 10006, msg: 'no connection' });
        logger.log('设备已断开');
      } else /* if (this.currentDevice && deviceId === this.currentDevice.deviceId)  */ {
        this.dispatchDeviceChage({ code: ST.CONNECTED, msg: 'connectioned' });
        logger.log('设备重连接');
      }
    });
  }

  /**
   * 创建连接并监听特征值
   * @private
   */
  async createConnect() {
    const { deviceId } = this.currentDevice;
    logger.log('[createConnect#0]', this.currentDevice);

    this.dispatchDataChage({ code: ST.CONNECTING });

    const connectRes = await this.connect(deviceId, deviceSku);
    logger.log('[createConnect#1]', connectRes);

    const servicesRes = await promisify(phera.getBLEDeviceServices)({ deviceId });
    logger.log('[createConnect#2]\n', deviceId, servicesRes);

    const currentServices = this.getCurrentServices(servicesRes);
    const serviceId = currentServices.serviceId;
    const characterRes = await promisify(phera.getBLEDeviceCharacteristics)({ deviceId, serviceId });
    logger.log('[createConnect#3]', characterRes);
    // todo: getBLEDeviceCharacteristics

    this.dispatchDeviceChage({ code: ST.CONNECTED, msg: 'connectioned' });
    await sleep(100);
    this.notifyConnectionStateChange();
    await sleep(100);

    logger.log('连接成功');


    return new Promise((resolve, reject) => {
      promisify(phera.notifyBLECharacteristicValueChange)({
          state: true, // 启用 notify 功能
          deviceId,
          // 这里的 serviceId 需要在 getBLEDeviceServices 接口中获取
          serviceId: currentServices.serviceId,
          // 这里的 characteristicId 需要在 getBLEDeviceCharacteristics 接口中获取
          characteristicId: currentServices.readCharacteristicId
        })
        .then(res => {
          this.notifyCharacteristicValueChange();
          logger.log('[notifyBLECharacteristicValueChange]', res);
          resolve(res);
        })
        .catch(err => {
          logger.error(err);
          reject(err);
        });
    });
  }

  getCurrentServices(servicesRes) {
    return isIOS() ? QN.getIOSServices(servicesRes.services) : QN.getServices(servicesRes.services);
  }

  /**
   * 发现设备
   * @private
   */
  notifyDeviceFound() {
    phera.onBluetoothDeviceFound(async (res) => {
      logger.log('[onBluetoothDeviceFound]', res);
      const devices = res.devices;
      let device = this.findQNDevice(devices);
      if (device) {
        logger.log(`[QN-Scale]`, device);

        this.dispatchDeviceChage({ code: ST.DEVICE_FOUND, msg: 'device found', device });
        //如果是广播秤
        if (device.isBroadcast) {
          if (this.currentDevice && device.mac !== this.currentDevice.mac) return;
          this.currentDevice = device;
          this.onBroadcastScale(device); //广播秤处理逻辑
        } else { //蓝牙秤
          this.currentDevice = device;
          //扫到设备后停止扫描
          await this.stopDiscovery();
          const res = await this.createConnect();
          logger.log('notifyDeviceFound#createConnect', res);
        }
      }
    });
  }

  /**
   * 监听特征值变化
   * @private
   */
  notifyCharacteristicValueChange() {
    logger.log('[notifyCharacteristicValueChange#0]');

    let weight, code, encryptStr, _tempcode;
    phera.onBLECharacteristicValueChange(data => {
      if (this.stopped) {
        console.warn('bluetoothAdapter closed');
        return;
      }
      let { deviceId, value } = data;
      // logger.log(`[notifyCharacteristicValueChange#Change]`, deviceId, value);
      //转成16进制字符
      value = ab2hex(value);
      //根据特征值变化，获取对应的返回结果
      let result = QN.cmd({
        device: this.currentDevice,
        value
      });

      if (!result) return;

      code = result.code;
      isNumber(result.weight) && (weight = result.weight);
      result.encryptStr && (encryptStr = result.encryptStr);
      const personInfo = this.getPerson();

      if (code == 3001) { //测量中
        logger.log('[3001]', weight);
        this.dispatchDataChage({ code: ST.MEASURING, weight, encryptStr });
      } else if (code == 3002 && _tempcode === 3001) { //心率秤首次返回的命令
        logger.log('[3002]', weight, encryptStr);

        this.dispatchDataChage({ code: ST.CALC_PHYSICAL, weight, encryptStr });

        if (!this.checkPersonInfo(personInfo)) {
          setTimeout(() => {
            this.dispatchDataChage({ code: ST.REPORT_MID, weight, encryptStr });
          }, 100);
          return;
        }

        const { gender } = personInfo;
        this.decodeReport(encryptStr, personInfo, deviceSku, code).then(
          reportData => {
            const { reportDetail } = reportData;
            this.dispatchDataChage({ code: ST.REPORT_MID, weight, encryptStr, reportData });

            const { measurement } = JSON.parse(reportDetail);
            const { bodyfat, bmi } = this.achiveReport(measurement);

            result.cmdString = QN.itemCmdString({
              gender, //性别
              bodyfat, //体脂率，类型值为3
              bmi //bmi，类型值2
            });
          }, () => {
            this.dispatchDataChage({ code: ST.PAUSE });
          }
        );
      }

      if (result.cmdString) { //向蓝牙秤发送命令
        if (code == 3003 && _tempcode == 3002) {  /* [ST.REPORT, ST.PAUSE].indexOf(this._notifyCode) === -1 */
          //拿到对应的encryptStr加密字符

          if (!this.checkPersonInfo(personInfo)) {
            logger.warn('测量完成', weight, encryptStr);
            setTimeout(() => {
              this.dispatchDataChage({ code: ST.REPORT, weight, encryptStr });
            }, 100);
            return;
          }

          logger.log('测量完成', encryptStr);

          this.decodeReport(encryptStr, personInfo, deviceSku).then(
            reportData => {
              this.dispatchDataChage({ code: ST.REPORT, weight, encryptStr, reportData });
            }, () => {
              this.dispatchDataChage({ code: ST.PAUSE });
            }
          );

        }
        const ab = new ArrayBuffer(result.cmdString.length);
        const dv = new DataView(ab);

        result.cmdString.forEach((value, index) => {
          dv.setUint8(index, value);
        });

        phera.writeBLECharacteristicValue({
          deviceId,
          serviceId: result.serviceId,
          characteristicId: result.writeCharacteristicId,
          value: ab,
          success: () => {
            logger.log('写入成功');
          }
        });
      }

      _tempcode = code;
    });
  }

  /**
   * 广播秤测量
   * @param {*} device 
   */
  async onBroadcastScale(device) {
    let { code, encryptStr, weight } = device;
    logger.log('[onBroadcastScale]', code, weight, encryptStr);
    if (code == 3001) { //测量中
      this.dispatchDataChage({ code: ST.MEASURING, weight, encryptStr });
    } else if (code == 3003 && this._notifyCode < 3003) { //测量完成,得到encryptStr加密字符,并停止扫描
      logger.log('测量完成');
      this.dispatchDataChage({ code: ST.SCALE_READ, weight, encryptStr });
      //测量完成，停止扫描，使广播秤不再接收数据
      await this.stopDiscovery();

      this.dispatchDataChage({ code: ST.CALC_PHYSICAL, weight, encryptStr });

      let personInfo = this.getPerson();
      if (!this.checkPersonInfo(personInfo)) {
        this.dispatchDataChage({ code: ST.REPORT, weight });
        return;
      }

      this.decodeReport(encryptStr, personInfo, deviceSku).then(
        reportData => {
          this.discovery();
          this.dispatchDataChage({ code: ST.REPORT, weight, reportData });
        }, () => {
          this.discovery();
          this.dispatchDataChage({ code: ST.PAUSE });
        });
    }
  }

  /**
   * 重启插件
   * @returns {Promise<statusInfo>} 启动状态
   */
  async restart() {
    // await this.stop();
    return this.start();
  }

  /**
   * 停用插件
   */
  async stop() {
    logger.log('[stop]');
    if (this.started !== true) return;
    this.started = false;
    // this.stopped = true;
    this.currentDevice = null;
    return super.stop();
  }

  /**
   * 监听体脂秤状态变化
   * @public
   * @param {function(Object):void} cb 回调
   * @returns {function():void} 移除监听
   * 
   */
  onDataChange(cb) {
    return this.addEvent('__onDataChange', cb);
  }

  /**
   *  监听蓝牙状态变化
   * @param {function} cb 回调
   * @returns {function():void} 移除监听
   */
  onDeviceChange(cb) {
    return this.addEvent('__onDeviceChange', cb);
  }

  dispatchDataChage(params) {
    const { code } = params;
    this._notifyCode = code;

    const events = this.getEvents('__onDataChange');
    Array.isArray(events) && events.forEach(cb => cb(params));
  }

  dispatchDeviceChage(params) {
    const events = this.getEvents('__onDeviceChange');
    Array.isArray(events) && events.forEach(cb => cb(params));
  }

  /**
   * 解码测量数据
   * @public
   * @param {String} encryptStr 测量参数
   * @param {personInfo} personInfo 用户
   * @param {String} deviceSku sku信息
   * @returns {Promise<MeasureResult>} 测量结果
   * 
   */
  decodeReport(encryptStr, personInfo, deviceSku) {
    const hexString = String(encryptStr).toUpperCase();
    logger.log('decodeReport', Array.from(arguments));
    const params = {
      _mt: 'healthIot.parseData',
      familyId: personInfo.personId,
      height: parseInt(personInfo.height),
      birthDay: personInfo.birthday,
      gender: 1 - parseInt(personInfo.gender), // 
      originData: JSON.stringify({ hexString }),
      deviceSku
    };
    return new Promise((resolve, reject) => {
      request(mapi, params, 'POST', false).then(res => {
        let err;
        if (err = checkError(res)) {
          return reject(err);
        }
        const { content } = res;
        if (!content[0]) {
          return reject(reqfailmsg);
        }

        const { familyId, code, originData, parseResult } = content[0];
        const report = {
          code,
          personId: familyId,
          originData,
          reportDetail: parseResult
        };
        if (parseInt(personInfo.height) === 0) {
          let p = JSON.parse(parseResult);
          p.measurement = p.measurement.filter(v => v.type == 1);
          report.reportDetail = JSON.stringify(p);
        }
        resolve(report);
      }).catch(err => {
        logger.error('[decodeReport]', err);
        return Promise.reject(err);
      });
    });
  }

  checkPersonInfo(personInfo) {
    if (!personInfo || typeof personInfo !== "object") {
      console.warn('no personInfo');
      // alert('no personInfo');
      return false;
    }
    const { personId, height, birthday, gender } = personInfo;
    const p = !!personId && isNumber(height) && !!birthday && [0, 1].indexOf(gender) > -1;
    if (p === false) {
      // alert('no personInfo');
    }
    return p;
  }

  /**
   * 测量结束
   * @public
   * @returns {Promise<Object>}
   */
  async endMeasurement() {
    const stateRes = await promisify(phera.getBluetoothAdapterState)();
    const { discovering } = stateRes;

    if (discovering === false) {
      const connRes = await promisify(phera.getConnectedBluetoothDevices)();
      const { devices } = connRes;

      let device = this.findQNDevice(devices);
      if (!device) {
        this.discovery();
      }
    }
  }

  findQNDevice(devices) {
    if (!Array.isArray(devices)) return null;
    let device = null;
    for (let deviceItem of devices) {
      const { RSSI, advertisData, deviceId } = deviceItem;
      const localName = deviceItem.localName || deviceItem.name;

      const deviceObj = {
        deviceId,
        localName,
        RSSI,
        advertisData: ab2hex(advertisData)
      };
      device = QN.getDevice(deviceObj) || null; //获取蓝牙设备
      if (device) {
        break;
      }
    }
    return device;
  }

  /**
   * 设置personData
   * @param {fatScaleProps} props 初始化参数
   */
  async setPerson(personInfo) {
    this.curPerson = Object.assign({}, this.curPerson, personInfo);
  }

  getPerson() {
    return this.curPerson;
  }

  getConnectedDevice() {
    if (!this.currentDevice) return null;
    const { mac, isBroadcast } = this.currentDevice;
    return {
      deviceSku,
      mac,
      isBroadcast
    };
  }

  getDeviceSku() {
    return deviceSku;
  }

  achiveReport(measurementArr) {
    const obj = {};
    const p = {
      1: 'weight',
      2: 'bmi',
      3: 'bodyfat',
      4: 'subfat',
      5: 'visfat',
      6: 'water',
      7: 'muscle',
      8: 'bone',
      9: 'bmr',
      10: 'bodyShape',
      11: 'protein',
      12: 'lbm',
      13: 'muscleMass',
      14: 'bodyAge',
      15: 'score',
      16: 'heartRate',
      17: 'heartIndex'
    };
    measurementArr.forEach(function(item) {
      let name;
      if (name = p[item.type]) {
        obj[name] = item.value;
      }
    });
    return obj;
  }

}

export default FatScale;
