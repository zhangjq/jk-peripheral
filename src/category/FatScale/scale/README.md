# 说明

|方法|说明
|:-|:-
|formatItemData|把指标数组转成指标对象，同小程序插件的使用一样
|reportData|把指标对象转成指标报告，同小程序插件的使用一样，不同的是，不以 promise的方式返回，而是直接返回指标结果
|deepReport|把指标转成深度报告，同小程序插件的使用一样
|getDevice|获取云康宝的设备对象,返回 null 则不是云康宝设备
|itemCmdString|根据参数获取对应的指标命令，显示到体脂秤上，心率秤才需要
|cmd|根据特征值获取对应的返回参数
|getServices|获取设备的服务 id


### getDevice方法使用

```javascript

let device = QN.getDevice({
	deviceId, //设备的 id
    localName, //设备的蓝牙名
    RSSI,      //设备的信号强度
	advertisData //advertisData的16进制字符
})


```

### getDevice返回值

```javascript

//蓝牙秤的返回值
{
	localName,//设备蓝牙名
	mac,//设备 mac 地址
	internal_model,//内部型号
	rssi,//信号强度
	deviceId,//设备 id
	advertisData，//advertisData的16进制字符
	isBroadcast,//是否为广播秤
}

//广播秤返回值
//code为3001时，测量中
{
	code: 3001,
	weight,//实时体重值
	isBroadcast,//是否为广播秤.
}

//code为3003，测量完成
{
	code: 3003
	encryptStr,//加密的字符
	weight,//实时体重值
	isBroadcast,//是否为广播秤.
}

```


### cmd方法使用

```javascript

let result = QN.cmd({
	device,//getDevice获取到的设备
	value,//特征值对应的16进制字符
})


```

### cmd 方法返回值

```javascript

{
	code: 3000,//回复体脂秤，确认开始接收数据，要向体脂发送对应的 cmdString命令
    msg: '',
    cmdString,//需要向体脂秤发送的命令16进制字符
    serviceId,//serviceId，发送命令需要用到
    readCharacteristicId，
	writeCharacteristicId，//writeCharacteristicId，发送命令需要用到
}

{
	code: 3001,//测量中
	msg: '测量中',
	weight//实时体重值
}

{
	code: 3002,//心率秤的首次加密字符生成
	msg: '',
	encryptStr,//加密字符
	serviceId,//serviceId，发送命令需要用到
    readCharacteristicId，
	writeCharacteristicId，//writeCharacteristicId，发送命令需要用到
}

{
	code: 3003,//测量完成
	msg: '测量完成',
	cmdString,//需要向体脂秤发送的命令16进制字符
	encryptStr,//加密字符
	serviceId,//serviceId，发送命令需要用到
    readCharacteristicId，
	writeCharacteristicId，//writeCharacteristicId，发送命令需要用到
}

```


### itemCmdString使用

```javascript

let cmdString = QN.itemCmdString({
	gender: 1,//性别
    bodyfat: 19.2,//体脂率，类型值为3
    bmi: 20//bmi，类型值2
})
//得到的cmdString是要发送到体脂秤的16进制字符

```

### getServices使用

```javascript
 
//参数对应微信方法wx.getBLEDeviceServices中的 res.services数组列表
let currentServices = QN.getServices(res.services)
 
//返回值
{
 	serviceId,//serviceId，发送命令需要用到
    readCharacteristicId，//readCharacteristicId，发送命令需要用到
	writeCharacteristicId，//writeCharacteristicId，发送命令需要用到
}
 
```


### reportData使用

```javascript

let obj = {
  birthday: "1990-01-01",
  weight: 60,
  bmi: 20,
  height: 170,
  gender: 1,
  bodyfat: 19.8,
  water: 57.9,
  protein: 18.32,
  bmr: 1568,
  bodyAge: 24,
  bodyShape: 4,
  bone: 2.6,
  muscle: 55.7,
  subfat: 15.8,
  lbm: 41.56,
  visfat: 6,
  heartRate: 61,
  heartIndex: 3.5,
  muscleMass: 53.51
}

let result = QN.reportData(obj)

console.log('result', result)


```













