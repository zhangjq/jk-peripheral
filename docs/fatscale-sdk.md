﻿
## 前端接口

#### 启动、停止、重启插件
 
```javascript
import FatScale, { MEASURE_STATUS } from "equipme-plugins/FatScale";

const height = 170, gender = 1, birthday = '1991-01-01', presonId = '2238150939020708';
const fatScale = new FatScale();
fatScale.setPerson({ height, gender, birthday, presonId });
// 打开蓝牙适配器
const { errCode } = await fatScale.start();
// 搜索并连接体脂秤
const { errCode } = await fatScale.discovery();
// const { errCode } = await fatScale.stop();
// const { errCode } = await fatScale.restart();
``` 
------------------

#### 蓝牙状态码
| errCode | errMsg               | 说明                                          |
| ------- | -------------------- | --------------------------------------------- |
| 0       | ok                   | 正常                                          |
| 10000   | not init             | 未初始化蓝牙适配器                            |
| 10001   | not available        | 当前蓝牙适配器不可用                          |
| 10002   | no device            | 没有找到指定设备                              |
| 10003   | connection fail      | 连接失败                                      |
| 10004   | no service           | 没有找到指定服务                              |
| 10005   | no characteristic    | 没有找到指定特征值                            |
| 10006   | no connection        | 当前连接已断开                                |
| 10007   | property not support | 当前特征值不支持此操作                        |
| 10008   | system error         | 其余所有系统上报的异常                        |
| 10009   | system not support   | Android 系统特有，系统版本低于 4.3 不支持 BLE |
| 10012   | operate time out     | 连接超时                                      |
| 10013   | invalid_data         | 连接 deviceId 为空或者是格式不正确            |
| 10031   | old version          | 版本过低                                      |

------------------
#### 蓝牙状态监听

```javascript
let offDeviceChange = fatScale.onDeviceChange(data => {
  const { errCode } = data;
  switch (errCode) {
    case 0:
      // 蓝牙就绪
      break;
    case 10001:
      // 当前蓝牙适配器不可用
    case 10006:
      // 当前连接已断开
      break;
    }
});
```
---------------------
#### 体脂秤状态监听

```javascript
import { MEASURE_STATUS } from "equipme-plugins/FatScale";
let offDataChange = fatScale.onDataChange(data => {
  const { code, weight } = data;
  switch (code) {
    case MEASURE_STATUS.PAUSE:
      // ...
      break;
    case MEASURE_STATUS.REPORT_MID:
    case MEASURE_STATUS.REPORT:
      const { reportId, encryptStr, reportDetail } = data.reportData;
      // ...
      break;
    }
});
```
###### 状态码
| MEASURE_STATUS | code | description            |
| -------------- | ---- | ---------------------- |
| CONNECTING     | 2009 | 正在连接               |
| PAUSE          | 3000 | 空闲                   |
| MEASURING      | 3001 | 测量中                 |
| SCALE_READ_MID | 3002 | [心率秤]第一次返回     |
| CALC_PHYSICAL  | 3004 | 计算体质数据           |
| REPORT_MID     | 3005 | [心率秤]第一次测量报告 |
| SCALE_READ     | 3003 | 秤返回                 |
| CALC_HEART     | 3006 | [心率秤]计算心率数据   |
| REPORT         | 3010 | 获得测量报告           |

------------------ 

#### 解码报告
```javascript
  const personifo = fatScale.getPerson(); // { personId, height, birthDay, gender }
  const { code, personId, encryptStr, reportDetail } = await fatScale.decodeReport( encryptStr, personInfo, deviceSku );
```
------------------   

#### 测量详情

```javascript
const reportDetail = "{\"mac\":\"60:04:34:56:32:12\",\"weight\":60.50,\"measurement\":[{\"type\":1,\"value\":60.50},{\"type\":2,\"value\":19.10},{\"type\":3,\"value\":18.40},{\"type\":4,\"value\":17.50},{\"type\":5,\"value\":1},{\"type\":6,\"value\":56.00},{\"type\":7,\"value\":47.60},{\"type\":8,\"value\":2.96},{\"type\":9,\"value\":1436.00},{\"type\":10,\"value\":5},{\"type\":11,\"value\":19.79},{\"type\":12,\"value\":49.37},{\"type\":13,\"value\":46.41},{\"type\":14,\"value\":18},{\"type\":15,\"value\":90.00},{\"type\":16,\"value\":100},{\"type\":17,\"value\":4.10}]}";
const { measurement, weight, mac } = JSON.parse(reportDetail);
// measurement 转义
const measurementList = fatScale.achiveReport(measurement);

// 获取深度报告（自定义 FatScale/kpi/codes/deepReport.js）
```

------------------ 

#### 设置测量Person
```javascript
fatScale.setPerson({ height, gender, birthday, presonId });
```
------------------ 
 
#### 连接设备deviceInfo
```javascript
// deviceSku 设备类型类型, deviceId编号 设备（Android、IOS 获取的值不相同）
// 对于广播秤（没有长连）表示的是上次发送测量的设备
// isBroadcast 是否是广播秤
const { deviceSku, deviceId, isBroadcast } = fatScale.getConnectedDevice();
```
------------------ 
